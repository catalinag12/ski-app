package com.example.skixpert_trackingskiperformance;

import android.content.Context;
import android.content.ReceiverCallNotAllowedException;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ResortAdapter extends  RecyclerView.Adapter<ResortAdapter.ResortViewHolder>{


    private Context mCtx;
   private List<Resort> resortList;
   private OnItemClickListener mListener;
   private double [] distante;

   public interface  OnItemClickListener{
       void onItemClick(int position);
   }

   public void setOnItemClickListener(OnItemClickListener listener){
       mListener=listener;
   }

    public ResortAdapter(Context mCtx, List<Resort> resortList) {
        this.mCtx = mCtx;
        this.resortList = resortList;

    }

    public ResortAdapter(Context mCtx, List<Resort> resortList,double distante []) {
        this.mCtx = mCtx;
        this.resortList = resortList;
        this.distante=distante;
    }

    @NonNull
    @Override
    public ResortViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(mCtx);
        View view= inflater.inflate(R.layout.list_resorts, null);
        ResortViewHolder holder=new ResortViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ResortViewHolder resortViewHolder, int i) {


        Resort resort=resortList.get(i);
        //bound data
        resortViewHolder.tvLocation.setText(resort.getLocation());
        resortViewHolder.tvRating.setText(String.valueOf(resort.getRating()));
resortViewHolder.tvDistanta.setText(String.format("%.2f",distante[i])+"km");
    //    resortViewHolder.tvLatitude.setText(String.valueOf(resort.getLatitude()));
        String location=resort.getLocation();


        Glide.with(mCtx)
                .load(resort.getImage())
                .into((ImageView) resortViewHolder.imageView);


            String url="https://api.openweathermap.org/data/2.5/weather?q="+location+"&appid=a60d609351e91ee66d791cae21d45fe6";

            JsonObjectRequest jor=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject mainObject=response.getJSONObject("main");
                        JSONArray array=response.getJSONArray("weather");
                        JSONObject object=array.getJSONObject(0);

                        String temp=String.valueOf(mainObject.getDouble("temp"));

                        String icon=object.getString("icon");



                        resortViewHolder.tvTemperature.setText(temp +"°C");


                        Glide.with(mCtx).load("https://openweathermap.org/img/w/"+icon+".png").into(resortViewHolder.imgState);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            RequestQueue queue= Volley.newRequestQueue(mCtx);
            queue.add(jor);

        }



    @Override
    public int getItemCount() {
        return resortList.size();
    }


    //for the view Holder

    class ResortViewHolder extends RecyclerView.ViewHolder {

       ImageView imgState;
       TextView tvTemperature;
        View imageView;
        TextView tvLocation, tvRating,tvLatitude, tvDistanta;


        public ResortViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView=itemView.findViewById(R.id.imageView);
            tvLocation=itemView.findViewById(R.id.tvLocation);
            tvRating=itemView.findViewById(R.id.tvRating);
            tvDistanta=itemView.findViewById(R.id.distanta);

           // tvLatitude=itemView.findViewById(R.id.latitude);
            imgState=itemView.findViewById(R.id.imgState);
            tvTemperature=itemView.findViewById(R.id.tvTemp);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener!=null){
                        int position=getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            mListener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
