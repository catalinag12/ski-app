package com.example.skixpert_trackingskiperformance.Common;

import android.location.Location;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonWeather {

    public static final String APP_ID="a60d609351e91ee66d791cae21d45fe6";

    public static Location current_location=null;

    public static String convertUnixToDate(long dt) {
        Date date=new Date(dt*1000L);
        SimpleDateFormat sdf=new SimpleDateFormat("HH:mm dd EEE MM yyyy ");
        String formatted=sdf.format(date);
        return formatted;
    }

    public static String  convertUnixToHour(long dt) {
        Date date=new Date(dt*1000L);
        SimpleDateFormat sdf=new SimpleDateFormat("HH:mm EEE MM yyyy ");
        String formatted=sdf.format(date);
        return formatted;
    }
}
