package com.example.skixpert_trackingskiperformance;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class DiaryItemActivity extends AppCompatActivity implements EditorView{
    TextView  tvMaxSp,tvAvgSp,tvMaxA,tvMinA,tvDist,tvDur;
    EditText tvRat,tvDesc;

    String description;
    Float max_speed, avg_speed;
    double max_alt,min_alt, distance;
    int id,duration,rating,user_id;

    EditorPresenter presenter;
    ProgressDialog progressDialog;
    RatingBar ratingBar;

    Menu actionMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary_item);

        ratingBar=findViewById(R.id.ratingBar);
        tvDesc=findViewById(R.id.tvDescr);
        tvMaxSp=findViewById(R.id.tvMaxSp);
        tvAvgSp=findViewById(R.id.tvAvgSp);
        tvMinA=findViewById(R.id.tvMinA);
        tvMaxA=findViewById(R.id.tvMaxA);
        tvDist=findViewById(R.id.tvDista);
        tvDur=findViewById(R.id.tvDur);
        tvRat=findViewById(R.id.tvRat);

        progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("Please wait...");
        //presenter=new DiaryPresenter(this);

        presenter=new EditorPresenter(this);


        ratingBar.setMax(10);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                tvRat.setText(String.valueOf(Math.round(ratingBar.getRating())));

            }
        });
        Intent intent=getIntent();
        id=intent.getIntExtra("id_measure",0);
        description=intent.getStringExtra("description");
        max_speed=intent.getFloatExtra("max_speed",0);
        avg_speed=intent.getFloatExtra("avg_speed",0);
        min_alt=intent.getDoubleExtra("min_alt",0);
        max_alt=intent.getDoubleExtra("max_alt",0);
        distance=intent.getDoubleExtra("distance",0);
        rating=intent.getIntExtra("rating",0);
        duration=intent.getIntExtra("duration",0);
        user_id=intent.getIntExtra("id_user",0);
        sendDataFromIntentExtra();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.update:
                String descrip=tvDesc.getText().toString();
                String ratingg=tvRat.getText().toString();
                presenter.updateDiaryItem(id,descrip,Integer.parseInt(ratingg) );
                return true;
            case R.id.edit:
                editMode();
                actionMenu.findItem(R.id.save).setVisible(false);
                actionMenu.findItem(R.id.update).setVisible(true);
                actionMenu.findItem(R.id.delete).setVisible(false);
                actionMenu.findItem(R.id.save).setVisible(false);
                return true;

            case R.id.delete:
                AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
                alertDialog.setTitle("Confirm");
                alertDialog.setMessage("Are you sure? ");
                alertDialog.setNegativeButton("Yes", (dialog, which) ->{
                        dialog.dismiss();
                        presenter.deleteDiaryItem(id);});
                alertDialog.setPositiveButton("Cancel",(dialog, which) -> {
                    dialog.dismiss();
                });
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_editor,menu);
        actionMenu=menu;


        if(id!=0){
            actionMenu.findItem(R.id.edit).setVisible(true);
            actionMenu.findItem(R.id.delete).setVisible(true);
            actionMenu.findItem(R.id.save).setVisible(false);
            actionMenu.findItem(R.id.update).setVisible(false);


        }
        else{
            actionMenu.findItem(R.id.update).setVisible(false);
            actionMenu.findItem(R.id.delete).setVisible(false);
            actionMenu.findItem(R.id.save).setVisible(true);
            actionMenu.findItem(R.id.edit).setVisible(false);

        }
        return true;
    }

    private void sendDataFromIntentExtra() {
        if(id!=0){
            if(!MeasurementsFragment.metrics) {
                tvDesc.setText(description);
                tvMaxSp.setText(max_speed.toString() + " km/h");
                tvAvgSp.setText(avg_speed.toString() + " km/h");
                tvMaxA.setText(String.valueOf(max_alt) + " meters");
                tvMinA.setText(String.valueOf(min_alt) + " meters");
                tvDist.setText(String.valueOf(distance) + " km's");
                tvDur.setText(String.valueOf(duration) + " minutes");
                tvRat.setText(String.valueOf(rating));

            }
            else{
                tvDesc.setText(description);
                tvMaxSp.setText(max_speed.toString() + " miles/h");
                tvAvgSp.setText(avg_speed.toString() + " miles/h");
                tvMaxA.setText(String.valueOf(max_alt) + " feet");
                tvMinA.setText(String.valueOf(min_alt) + " feet");
                tvDist.setText(String.valueOf(distance) + " km's");
                tvDur.setText(String.valueOf(duration) + " minutes");
                tvRat.setText(String.valueOf(rating));
            }

            getSupportActionBar().setTitle("Update diary item");
            readMode();
        }
        else{
            editMode();
        }


    }

    private void readMode() {
        tvRat.setFocusableInTouchMode(false);
        tvDesc.setFocusableInTouchMode(false);
        tvRat.setFocusable(false);
        tvDesc.setFocusable(false);
    }
    private void editMode(){
        tvRat.setFocusableInTouchMode(true);
        tvDesc.setFocusableInTouchMode(true);

    }

    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void onRequestSuccess(String message) {
        Toast.makeText(DiaryItemActivity.this,message,Toast.LENGTH_LONG).show();
    finish();
    }

    @Override
    public void onRequestError(String message) {
        Toast.makeText(DiaryItemActivity.this,message,Toast.LENGTH_LONG).show();

    }
}
