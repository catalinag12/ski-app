package com.example.skixpert_trackingskiperformance;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class AlertReceiver2 extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationHelper notificationHelper=new NotificationHelper(context);

        NotificationCompat.Builder nb2=notificationHelper.getChannelNotification2();

        notificationHelper.getManager().notify(2,nb2.build());
    }
}
