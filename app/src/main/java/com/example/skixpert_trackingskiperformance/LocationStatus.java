package com.example.skixpert_trackingskiperformance;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CONTROL_LOCATION_UPDATES;

public class LocationStatus extends AppCompatActivity {

    TextView tvLatitude;
    TextView tvLongitude, tvAltitude, tvAccuracy, tvTimestamp;
    FusedLocationProviderClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_status);

        tvLatitude = findViewById(R.id.tvLatitude);
        tvLongitude = findViewById(R.id.tvLongitude);
        tvAltitude = findViewById(R.id.tvAltitudeStatus);
        tvAccuracy = findViewById(R.id.tvAccuracy);
        tvTimestamp = findViewById(R.id.tvTimestamp);

        Date timeNow= Calendar.getInstance().getTime();
        tvTimestamp.setText(timeNow.toString());




        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1);
        client = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(LocationStatus.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {

            return;
        }


        client.getLastLocation().addOnSuccessListener(LocationStatus.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    CLocation myLocation=new CLocation(location);
                    tvLongitude.setText(String.valueOf(myLocation.getLongitude()));
                    tvLatitude.setText(String.valueOf(myLocation.getLatitude()));
                    tvAltitude.setText(String.valueOf(myLocation.getAltitude()));
                    tvAccuracy.setText(String.valueOf(myLocation.getAccuracy()));


                }
                else{

                    Toast.makeText(getApplicationContext(),"No location was found",Toast.LENGTH_LONG).show();
                }
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.menu_done,menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_done:
                Intent intent=new Intent(getApplicationContext(),HomePageActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
