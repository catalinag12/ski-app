package com.example.skixpert_trackingskiperformance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText name, email, password, edtConfirmation;
    Button btnRegister;
    ProgressBar pbLoading;
    static String IP="192.168.0.178";
    private static String URL_Register="http://192.168.0.178/licenta/register.php";
    boolean isValid=true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        edtConfirmation=findViewById(R.id.edtConfirm);
        email=findViewById(R.id.edtEmail);
        name=findViewById(R.id.edtName);
        password=findViewById(R.id.edtPassword);
        btnRegister=findViewById(R.id.btnRegister);
        pbLoading=findViewById(R.id.pbLoading);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValid=true;
                if(TextUtils.isEmpty((email).getText()) || TextUtils.isEmpty(name.getText()) || TextUtils.isEmpty(password.getText()) || TextUtils.isEmpty(edtConfirmation.getText())){
                    if(TextUtils.isEmpty(name.getText())){
                        name.setError("Please insert your name.");
                        isValid=false;
                    }
                    if(TextUtils.isEmpty(email.getText())){
                        email.setError("Please insert your email.");
                        isValid=false;

                    }
                    if(TextUtils.isEmpty(password.getText())){
                        password.setError("Please choose a password.");
                        isValid=false;

                    }
                    if(TextUtils.isEmpty(edtConfirmation.getText())){
                        edtConfirmation.setError("Please confirm password.");
                        isValid=false;

                    }


                }
                if(password.getText().toString().length()<4){
                    password.setError("The password is too short! ");
                    isValid=false;

                }
                if(!(password.getText().toString() .equals( edtConfirmation.getText().toString()))){
                    edtConfirmation.setError("The passwords do not match!");
                    isValid=false;

                }
                if(isValid) {
                    Register();
                }

            }
        });

    }

    public void Register(){
        pbLoading.setVisibility(View.VISIBLE);
        btnRegister.setVisibility(View.GONE);
        final String name=this.name.getText().toString().trim();
        final String email=this.email.getText().toString().trim();
        final String password=this.password.getText().toString().trim();


        StringRequest stringRequest=new StringRequest(Request.Method.POST, URL_Register,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.names().get(0).equals("success")){
                                Toast.makeText(RegisterActivity.this,"Registered successfully",Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                            }
                            else{
                                Toast.makeText(getApplicationContext(), "Error" +jsonObject.getString("error"), Toast.LENGTH_SHORT).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(RegisterActivity.this,"Register error json exception" +e.toString(),Toast.LENGTH_LONG).show();
                            pbLoading.setVisibility(View.GONE);
                            btnRegister.setVisibility(View.VISIBLE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterActivity.this,"Register error" +error.toString(),Toast.LENGTH_LONG).show();
                        pbLoading.setVisibility(View.GONE);
                        btnRegister.setVisibility(View.VISIBLE);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<String,String>();
                params.put("name",name);
                params.put("email",email);
                params.put("password",password);

                return params;
            }
        };

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
