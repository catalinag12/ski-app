package com.example.skixpert_trackingskiperformance;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.api.Api;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditorPresenter {

    private EditorView view;

    public EditorPresenter(EditorView view) {
        this.view = view;
    }

     void saveToDB(final int user_id,final float max_speed, final float avg_speed, final double max_altitude,final double min_altitude,final double distance, final int duration,final int rating, final String description) {

        view.showProgress();

        ApiInterface apiInterface=ApiClient.getApiClient().create(ApiInterface.class);
      //  Log.d(TAG, " user_id: " + user_id + "max_speed: " + max_speed + ", avg_speed: " + avg_speed + ", max_altitude: " + max_altitude + ", min_altitued: " + min_altitude + ", distance: " + distance + ", duration: " + duration);
        Call<Measurements> call=apiInterface.saveToDB(user_id,max_speed, avg_speed, max_altitude, min_altitude, distance, duration,rating,description);

        call.enqueue(new Callback<Measurements>() {
            @Override
            public void onResponse(@NonNull Call<Measurements> call, @NonNull Response<Measurements> response) {
                view.hideProgress();
                if(response.isSuccessful()  && response.body()!=null ) {

                    Boolean success = response.body().getSuccess();
                    if (success) {
                        view.onRequestSuccess(response.body().getMessage());


                    } else {
                        view.onRequestError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Measurements> call, @NonNull Throwable t) {
            view.hideProgress();
            view.onRequestError(t.getLocalizedMessage());}
        });
    }


    void updateDiaryItem(int id, String description, int rating){
        view.showProgress();
        ApiInterface apiInterface=ApiClient.getApiClient().create(ApiInterface.class);
        Call<Measurements> call=apiInterface.updateDiaryItem(id,rating,description);
        call.enqueue(new Callback<Measurements>() {
            @Override
            public void onResponse(@NotNull Call<Measurements> call, Response<Measurements> response) {
                view.hideProgress();
                if(response.isSuccessful() && response.body()!=null){

                    Boolean success=response.body().getSuccess();
                    if(success){
                    view.onRequestSuccess(response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<Measurements> call, Throwable t) {
                view.hideProgress();
                view.onRequestError("DE CE ?" + t.getLocalizedMessage());

            }
        });

    }

    void deleteDiaryItem(int id){
        view.showProgress();
        ApiInterface apiInterface=ApiClient.getApiClient().create(ApiInterface.class);
        Call<Measurements> call= apiInterface.deleteDiaryItem(id);
        call.enqueue(new Callback<Measurements>() {
            @Override
            public void onResponse(@NotNull Call<Measurements> call, Response<Measurements> response) {
                view.hideProgress();
                if(response.isSuccessful() && response.body()!=null){
                    Boolean success=response.body().getSuccess();
                    if(success){
                        view.onRequestSuccess(response.body().getMessage());
                    }
                    else{
                        view.onRequestError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<Measurements> call, Throwable t) {
    view.hideProgress();
        view.onRequestError(t.getLocalizedMessage());
            }
        });
    }
}
