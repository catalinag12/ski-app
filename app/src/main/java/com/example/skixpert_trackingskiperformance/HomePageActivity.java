package com.example.skixpert_trackingskiperformance;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomePageActivity extends AppCompatActivity  implements View.OnClickListener {

    TextView tv1,tv2,tv3;
    private BottomSheetDialog bottomSheetDialog;
    private static final String TAG =HomePageActivity.class.getSimpleName() ;
    EditText name, email;
    Button btnLogout, btnEdit ;
    SessionManager sessionManager;
    String getId;
    TextView tvCountdown,tvReady;
    Button btnWeather;

    Uri image_uri;
    public static int id_user_good;

    FragmentManager manager=getSupportFragmentManager();
    FragmentTransaction t=manager.beginTransaction();
    final MeasurementsFragment m4=new MeasurementsFragment();

    TextView skip, keep;
    Button btnReview;

    ObjectAnimator objectAnimator, objectAnimator1;

    static String IP="192.168.0.178";
    static String URL_READ="http://"+IP+"/licenta/read_detail.php";
    Menu action;
    static String URL_EDIT="http://"+IP+"/licenta/edit_detail.php";
    Bitmap bitmap;
    ImageView profile_image;
    static String URL_UPLOAD="http://"+IP+"/licenta/upload.php";
    ProgressBar pbCountdown;
    CountDownTimer countDownTimer;
    int i=0;


    String this_id;

    ImageView imgUser;
    ImageView imgEmail;
    BottomNavigationView bottomNavigationView;


    TextView tvName;


    ImageView btnMeasure;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        tvName=findViewById(R.id.tvUser);
        name=findViewById(R.id.edtName);
        email=findViewById(R.id.edtEmail);

        btnMeasure=findViewById(R.id.btnMeasure);
        btnReview=findViewById(R.id.btnReview);
        btnWeather=findViewById(R.id.btnWeather);

        bottomSheetDialog=new BottomSheetDialog(HomePageActivity.this);
        View bottomSheetDialogView=getLayoutInflater().inflate(R.layout.dialog_layout,null);
        bottomSheetDialog.setContentView(bottomSheetDialogView);

        pbCountdown=findViewById(R.id.pbCountdown);
        pbCountdown.setVisibility(View.GONE);
        pbCountdown.setProgress(i);
tvCountdown=findViewById(R.id.tvCountdown);
tvCountdown.setVisibility(View.GONE);
tvReady=findViewById(R.id.tvGetReady);
tvReady.setVisibility(View.GONE);

tv1=findViewById(R.id.tv1);
        tv2=findViewById(R.id.tv2);
        tv3=findViewById(R.id.tv3);


View keep=bottomSheetDialogView.findViewById(R.id.keep);
View skip=bottomSheetDialogView.findViewById(R.id.skip);
keep.setOnClickListener(this);
skip.setOnClickListener(this);
btnMeasure.setOnClickListener(this);




        imgEmail=findViewById(R.id.imgEmail);
        imgUser=findViewById(R.id.imgUser);

        bottomNavigationView=findViewById(R.id.bottom_navigation);
        pbCountdown.setMax(10);



     String id=getIntent().getStringExtra("user_id");
     this_id=id;
     if(id!=null){
      int id_user_good1=Integer.parseInt(id);
     id_user_good=id_user_good1;}


     objectAnimator=ObjectAnimator.ofFloat(btnWeather,"rotation",360);
        objectAnimator1=ObjectAnimator.ofFloat(btnMeasure,"rotation",360);




        countDownTimer=new CountDownTimer(10000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                pbCountdown.setProgress(i);
                i++;
                tvCountdown.setTextSize(30);
                tvCountdown.setText( String.valueOf(millisUntilFinished/1000));
            }

            @Override
            public void onFinish() {
                i++;
                pbCountdown.setProgress(10);
                pbCountdown.setVisibility(View.GONE);
                tvCountdown.setVisibility(View.GONE);
                tvReady.setVisibility(View.VISIBLE);

                FragmentManager fm=getSupportFragmentManager();
                MeasurementsFragment fragment=new MeasurementsFragment();
                fm.beginTransaction().replace(R.id.containerHP,fragment).commitAllowingStateLoss();

            }
        };



//        btnMeasure.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//
//
//
//            }
//});

     sessionManager=new SessionManager(this);
     sessionManager.checkLogin();

        HashMap<String,String> user=sessionManager.getUserDetail();
    getId=user.get(sessionManager.ID);



    Menu menu=bottomNavigationView.getMenu();
    MenuItem menuItem=menu.getItem(0);
    menuItem.setChecked(true);

    bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch(item.getItemId()) {


                case R.id.nav_diary:
                    Intent intent=new Intent(getApplicationContext(),DiaryActivity.class);
                    startActivity(intent);
                    break;


                case R.id.nav_map:
                    Intent intent2=new Intent(getApplicationContext(),MapActivity.class);
                    startActivity(intent2);
                    break;

                case R.id.nav_statistics:
                    Intent intent3=new Intent(getApplicationContext(), SettingsActivity.class);
                    startActivity(intent3);
                    break;

                case R.id.nav_resorts:
                    Intent intent4=new Intent(getApplicationContext(),ResortActivity.class);
                    startActivity(intent4);
                    break;

                case R.id.nav_home:
                    break;



            }
            return false;
        }
    });



    btnWeather.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            objectAnimator.setDuration(4000);
            objectAnimator.start();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Intent intent5 =new Intent(getApplicationContext(),WeatherActivity.class);
                    startActivity(intent5);
                }
            }, 5000);

        }
    });



    btnReview.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            //Toast.makeText(getApplicationContext(),id,Toast.LENGTH_LONG).show();
            openDialog();
        }
    });
          btnLogout=findViewById(R.id.btnLogout);
          btnEdit=findViewById(R.id.btnEdit);
          profile_image=findViewById(R.id.profile_image1);

          btnEdit.setOnClickListener(new View.OnClickListener() {
              @Override
             public void onClick(View v) {
                 // chooseFile();
                  Intent intent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                  startActivityForResult(intent,0);
              }

          });



        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            sessionManager.logout();
            }
        });
    }

    public void openDialog() {
        ReviewDialog reviewDialog=new ReviewDialog();
        reviewDialog.show(getSupportFragmentManager(),"Review dialog");
    }

    private void getUserDetail(){
        final ProgressDialog progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        StringRequest stringRequest=new StringRequest(Request.Method.POST, URL_READ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.i(TAG,response.toString());
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String success=jsonObject.getString("success");
                            JSONArray jsonArray=jsonObject.getJSONArray("read");

                            if(success.equals("1")) {
                                for(int i=0;i<jsonArray.length();i++) {
                                    JSONObject object=jsonArray.getJSONObject(i);

                                    String strName=object.getString("name").trim();
                                    String strEmail=object.getString("email").trim();
                                    //adaugat acum
                                   String image=object.getString("image").trim();

                                    tvName.setText( strName);
                                    name.setText(strName);
                                  email.setText(strEmail);
                                   // Glide.with(getApplicationContext()).load(image).into(profile_image);
                                    Picasso.get().load(image).into(profile_image);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(),"Error reading details " +e.toString(),Toast.LENGTH_LONG).show();

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Error reading details " +error.toString(),Toast.LENGTH_LONG).show();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
               Map<String, String > params=new HashMap<>();
               params.put("id",getId);

                return params;
            }
        };

        RequestQueue requestQueue= Volley.newRequestQueue(this);
         requestQueue.add(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserDetail();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.menu_action,menu);

        action=menu;
        action.findItem(R.id.menu_save).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_edit:
                name.setFocusableInTouchMode(true);
                email.setFocusableInTouchMode(true);

                InputMethodManager imm= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(name, InputMethodManager.SHOW_IMPLICIT);

                action.findItem(R.id.menu_edit).setVisible(false);
                action.findItem(R.id.menu_save).setVisible(true);
                return true;

            case R.id.menu_save:
                SaveEditDetail();
                action.findItem(R.id.menu_edit).setVisible(true);
                action.findItem(R.id.menu_save).setVisible(false);

                name.setFocusableInTouchMode(false);
                email.setFocusableInTouchMode(false);

                name.setFocusable(false);
                email.setFocusable(false);
                return true;


            case R.id.menu_location:
                Intent intent=new Intent(getApplicationContext(),LocationStatus.class);
                startActivity(intent);
                return true;


            default:
                return super.onOptionsItemSelected(item);



        }

    }




    private void SaveEditDetail() {


        final String name=this.name.getText().toString().trim();
        final String email=this.email.getText().toString().trim();
        final String id=getId;

        final ProgressDialog progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Saving...");
        progressDialog.show();

        StringRequest stringRequest=new StringRequest(Request.Method.POST, URL_EDIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String success=jsonObject.getString("success");

                            if(success.equals("1")){
                              //  Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();
                                sessionManager.createSession(name,email,id);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(),"Error" +e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Error" +error.toString(),Toast.LENGTH_LONG).show();

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
               Map<String,String > params=new HashMap<>();
               params.put("name",name);
               params.put("email",email);
               params.put("id",id);


                return params;
            }
        };

        RequestQueue requestQueue=Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void chooseFile(){
//        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent,"Select Picture"),1);

        //ContentValues values=new ContentValues();
        //values.put(MediaStore.Images.Media.TITLE,"NEW PICTURE");
       // values.put(MediaStore.Images.Media.DESCRIPTION,"NEW PICTURE");
       // image_uri=getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,values);
        Intent camera_intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //camera_intent.putExtra(MediaStore.EXTRA_OUTPUT,image_uri);
        startActivityForResult(camera_intent,100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//  if(requestCode==1 && resultCode==RESULT_OK && data !=null && data.getData() !=null  ){
            super.onActivityResult(requestCode,resultCode,data);
            profile_image.setImageBitmap(null);
            Bitmap takenImage = (Bitmap) data.getExtras().get("data");
            profile_image.setImageBitmap(takenImage);

//        if(requestCode==1 && resultCode==RESULT_OK && data !=null && data.getData() !=null  ){
//            Uri filePath=data.getData();
//            try {
//                bitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),filePath);
//                profile_image.setImageBitmap(bitmap);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//
//
           UploadPicture(getId,getStringImage(takenImage));
//
    }


    public Bundle getMyData(){
        Bundle hm=new Bundle();
        hm.putString("this_id",this_id);
        return hm;
    }
    private void UploadPicture(final String id , final String photo) {
        final ProgressDialog progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Uploading...");

        StringRequest stringRequest=new StringRequest(Request.Method.POST, URL_UPLOAD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                         Log.i(TAG,response.toString());
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String success=jsonObject.getString("success");
                            if(success.equals("1")){
                                Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Try again" + e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Try again" +error.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params=new HashMap<>();
                params.put("id",id);
                params.put("photo",photo);
            return params;
            }
        };
        RequestQueue requestQueue=Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
    public String getStringImage(Bitmap bitmap){

        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream()
;
    bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
    byte[] imageByteArray=byteArrayOutputStream.toByteArray();
    String encodedImage= Base64.encodeToString(imageByteArray,Base64.DEFAULT);
    return encodedImage;
    }


    @Override
    public void onClick(View v) {
        int id=v.getId();


        switch (id) {
            case R.id.btnMeasure:
                PropertyValuesHolder scalex = PropertyValuesHolder.ofFloat(View.SCALE_X,0.5f,1f);
                PropertyValuesHolder scaley = PropertyValuesHolder.ofFloat(View.SCALE_Y,0.5f,1f);
                PropertyValuesHolder alpha=PropertyValuesHolder.ofFloat(View.ALPHA,0f,1f);

                ObjectAnimator.ofPropertyValuesHolder(btnMeasure, scalex, scaley,alpha).start();
                objectAnimator1.setDuration(4000);
                objectAnimator1.start();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        btnWeather.setVisibility(View.GONE);
                        btnMeasure.setVisibility(View.GONE);
                        btnLogout.setVisibility(View.GONE);
                        name.setVisibility(View.GONE);
                        email.setVisibility(View.GONE);
                        profile_image.setVisibility(View.GONE);
                        imgUser.setVisibility(View.GONE);
                        imgEmail.setVisibility(View.GONE);
                        btnEdit.setVisibility(View.GONE);
                        btnReview.setVisibility(View.GONE);

                        bottomSheetDialog.show();
                    }
                }, 5000);

                break;

            case R.id.keep:
                tv1.setVisibility(View.GONE);
                tv2.setVisibility(View.GONE);
                tv3.setVisibility(View.GONE);

                tvName.setVisibility(View.GONE);
                pbCountdown.setVisibility(View.VISIBLE);
                tvCountdown.setVisibility(View.VISIBLE);

                countDownTimer.start();


                 bottomSheetDialog.cancel();
                break;
            case R.id.skip:
                FragmentManager fm1 = getSupportFragmentManager();
                MeasurementsFragment fragment1 = new MeasurementsFragment();
                fm1.beginTransaction().replace(R.id.containerHP, fragment1).commit();
                bottomSheetDialog.cancel();

                break;

        }
//        pbCountdown.setVisibility(View.VISIBLE);
//        tvCountdown.setVisibility(View.VISIBLE);
//
//        countDownTimer.start();




    }


}
