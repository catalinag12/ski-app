package com.example.skixpert_trackingskiperformance;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DiaryActivity extends AppCompatActivity implements DiaryView {
    private static final int INTENT_EDIT =200 ;
    RecyclerView recyclerView;

    SwipeRefreshLayout swipeRefresh;
    DiaryPresenter presenter;
    DiaryAdapter adapter;
    DiaryAdapter.ItemClickListener itemClickListener;
    List<Measurements> measurement;

    SharedPreferences preferences;
    EditorPresenter editorPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary);
        BottomNavigationView bottomNavigationView=findViewById(R.id.bottom_navigation);
        Menu menu=bottomNavigationView.getMenu();
        MenuItem menuItem=menu.getItem(1);
        menuItem.setChecked(true);
        measurement = new ArrayList<>();
        EditText editText=findViewById(R.id.editTextSearch);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                filter(s.toString());
            }
        });

        preferences=this.getSharedPreferences("MY_DATA",MODE_PRIVATE);

        recyclerView=findViewById(R.id.recycler_view);
        swipeRefresh=findViewById(R.id.swipe_refresh);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        presenter=new DiaryPresenter(this);
        presenter.getData();
        swipeRefresh.setOnRefreshListener(
                () -> presenter.getData()

        );

        String mSortSettings=preferences.getString("Sort","ascending");
        if(mSortSettings.equals("ascending")){
            Collections.sort(measurement,Measurements.BY_RATING_ASCENDING);
        }
        else if(mSortSettings.equals("descending")){
            Collections.sort(measurement,Measurements.BY_RATING_DESCENDING);

        }





        itemClickListener=((view,position) ->{
            int user_id=this.measurement.get(position).getUser_id();
            int id=this.measurement.get(position).getMeasurementId();
            float max_speed=measurement.get(position).getMax_speed();
            float avg_speed=measurement.get(position).getAvg_speed();
            double min_alt=measurement.get(position).getMin_altitude();
            double max_alt=measurement.get(position).getMax_altitude();
            double distance=measurement.get(position).getDistance();
            int duration=measurement.get(position).getDuration();
            int rating=measurement.get(position).getRating();
            String description=measurement.get(position).getDescription();

            Intent intent=new Intent(this,DiaryItemActivity.class);
            intent.putExtra("id_measure",id);
            intent.putExtra("id_user",user_id);
            intent.putExtra("max_speed",max_speed);
            intent.putExtra("avg_speed",avg_speed);
            intent.putExtra("min_alt",min_alt);
            intent.putExtra("max_alt",max_alt);
            intent.putExtra("distance",distance);
            intent.putExtra("duration",duration);
            intent.putExtra("rating",rating);
            intent.putExtra("description",description);


            startActivityForResult(intent,INTENT_EDIT);


            // Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();

        } );

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.nav_home:
                        Intent intentHome=new Intent(getApplicationContext(),HomePageActivity.class);
                        startActivity(intentHome);

                        break;

                    case R.id.nav_diary:
                        Intent intent=new Intent(getApplicationContext(),DiaryActivity.class);
                        startActivity(intent);
                        break;


                    case R.id.nav_map:
                        Intent intent2=new Intent(getApplicationContext(),MapActivity.class);
                        startActivity(intent2);
                        break;

                    case R.id.nav_statistics:
                        Intent intent3=new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.nav_resorts:
                        Intent intent4=new Intent(getApplicationContext(),ResortActivity.class);
                        startActivity(intent4);
                        break;

                }
                return false;
            }
        });
    }

    private void filter(String text) {
        ArrayList<Measurements> filteredList=new ArrayList<>();
        for(Measurements m: measurement){

            if(m.getDescription()!=null)
            if(m.getDescription().toLowerCase().contains(text.toLowerCase())){
                filteredList.add(m);
            }

        }
        adapter.filterList(filteredList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sort,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.action_sort){
            showSortDialog();
            return true;
        }
        if(id==R.id.action_sort_distance){
            showSortDialogDistance();
            return true;
        }if(id==R.id.action_sort_date){
            showSortDialogDate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showSortDialog() {
        String[] options ={"Ascending", "Descending"};
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Sort By");
        builder.setIcon(R.drawable.ic_sort);
        String mSortSettings=preferences.getString("Sort","ascending");

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which==0){//ascending
                    SharedPreferences.Editor editor=preferences.edit();
                    editor.putString("Sort","ascending");
                    editor.apply();
                    Collections.sort(measurement,Measurements.BY_RATING_ASCENDING);
                    adapter.notifyDataSetChanged();

                }
                if(which==1){//descending
                    SharedPreferences.Editor editor=preferences.edit();
                    editor.putString("Sort","descending");
                    editor.apply();
                    Collections.sort(measurement,Measurements.BY_RATING_DESCENDING);
                    adapter.notifyDataSetChanged();


                }
            }
        });
        builder.create().show();


    }




    private void showSortDialogDate() {
        String[] options ={"Ascending", "Descending"};
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Sort By Date");
        builder.setIcon(R.drawable.ic_sort);
        String mSortSettings=preferences.getString("Sort","ascending");

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which==0){//ascending
                    SharedPreferences.Editor editor=preferences.edit();
                    editor.putString("Sort","ascending");
                    editor.apply();
                    Collections.sort(measurement,Measurements.BY_DATE_ASCENDING);
                    adapter.notifyDataSetChanged();

                }
                if(which==1){//descending
                    SharedPreferences.Editor editor=preferences.edit();
                    editor.putString("Sort","descending");
                    editor.apply();
                    Collections.sort(measurement,Measurements.BY_DATE_DESCENDING);
                    adapter.notifyDataSetChanged();


                }
            }
        });
        builder.create().show();


    }
    private void showSortDialogDistance() {
        String[] options ={"Ascending", "Descending"};
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Sort By Distance");
        builder.setIcon(R.drawable.ic_sort);
        String mSortSettings=preferences.getString("Sort","ascending");

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which==0){//ascending
                    SharedPreferences.Editor editor=preferences.edit();
                    editor.putString("Sort","ascending");
                    editor.apply();
                    Collections.sort(measurement,Measurements.BY_DISTANCE_ASCENDING);
                    adapter.notifyDataSetChanged();

                }
                if(which==1){//descending
                    SharedPreferences.Editor editor=preferences.edit();
                    editor.putString("Sort","descending");
                    editor.apply();
                    Collections.sort(measurement,Measurements.BY_DISTANCE_DESCENDING);
                    adapter.notifyDataSetChanged();


                }
            }
        });
        builder.create().show();


    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==INTENT_EDIT && resultCode==RESULT_OK){
            presenter.getData();
        }
    }

    @Override
    public void showLoading() {
        swipeRefresh.setRefreshing(true);

    }

    @Override
    public void hideLoading() {
        swipeRefresh.setRefreshing(false);

    }

    @Override
    public void onGetResult(List<Measurements> measurements) {
        adapter=new DiaryAdapter(this,measurements,itemClickListener);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        measurement=measurements;
    }

    @Override
    public void onErrorLoading(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }
}
