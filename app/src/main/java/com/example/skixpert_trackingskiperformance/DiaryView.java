package com.example.skixpert_trackingskiperformance;

import java.util.List;

public interface DiaryView {
    void showLoading();
    void hideLoading();
    void onGetResult(List<Measurements> measurements);
    void onErrorLoading(String message);
}
