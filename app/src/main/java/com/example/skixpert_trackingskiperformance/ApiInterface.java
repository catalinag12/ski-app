package com.example.skixpert_trackingskiperformance;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface ApiInterface {

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("saveToDB.php")
    Call<Measurements> saveToDB(
            @Field("user_id") int user_id,
            @Field("max_speed") float max_speed,
            @Field("avg_speed") float avg_speed,
            @Field("max_altitude") double max_altitude,
            @Field("min_altitude") double min_altitude,
            @Field("distance") double distance,
            @Field("duration") int duration,
            @Field("rating") int rating,
            @Field("description") String description

    );

    @GET("measurements.php")
    Call<List<Measurements>> getMeasurements();

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
 @POST("updateDiaryItem.php")
 Call<Measurements> updateDiaryItem(
         @Field("measurement_id") int id,
         @Field("rating") int rating,
         @Field("description") String description

 );

 @FormUrlEncoded
 @POST("deleteItem.php")
 Call<Measurements> deleteDiaryItem(@Field("measurement_id") int id);

}
