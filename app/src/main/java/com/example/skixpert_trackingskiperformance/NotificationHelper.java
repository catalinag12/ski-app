package com.example.skixpert_trackingskiperformance;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NotificationHelper extends ContextWrapper {
    public static final String channelID = "channelID";
    public static final String channelName = "Channel Name";

    public static final String CHANNEL2="channel2";



    private NotificationManager mManager;



    public static void displayNotification(Context context, String title, String body )
    {
        Intent intent=new Intent(context,HomePageActivity.class);
        PendingIntent pendingIntent=PendingIntent.getActivity(context,
                100,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder mBuilder=
                new NotificationCompat.Builder(context,channelID)
                .setSmallIcon(R.drawable.icon_star)
                .setContentTitle(title)
                .setContentText(body)
                        .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH);
        NotificationManagerCompat mNotificationMngr=NotificationManagerCompat.from(context);
        mNotificationMngr.notify(1,mBuilder.build());
    }    public NotificationHelper(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationChannel channel = new NotificationChannel(channelID, channelName, NotificationManager.IMPORTANCE_HIGH);
        NotificationChannel channel2 = new NotificationChannel(CHANNEL2, channelName, NotificationManager.IMPORTANCE_HIGH);

        getManager().createNotificationChannel(channel);
        getManager().createNotificationChannel(channel2);

    }

    public NotificationManager getManager() {
        if (mManager == null) {
            mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

        return mManager;
    }



    public NotificationCompat.Builder getChannelNotification() {

        return new NotificationCompat.Builder(getApplicationContext(), channelID)
                .setContentTitle("WEATHER ")
                .setContentText("Weather status for today: " +SettingsActivity.descriereee)
                .setSmallIcon(R.drawable.ic_weather);
    }

    public NotificationCompat.Builder getChannelNotification2() {

        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL2)
                .setContentTitle("LUNCH TIME ")
                .setContentText("IT IS THE LAST RUN! PREPARE FOR LUNCH")
                .setSmallIcon(R.drawable.ic_arrow);
    }


}
