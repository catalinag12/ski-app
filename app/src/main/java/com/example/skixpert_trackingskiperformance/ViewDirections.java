package com.example.skixpert_trackingskiperformance;

import android.support.v4.app.FragmentActivity;


import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.skixpert_trackingskiperformance.HelperDirections.DirectionJSONParser;
import com.example.skixpert_trackingskiperformance.Retrofit.Common;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ViewDirections extends FragmentActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    private MarkerOptions mMarkerOptions;
    private LatLng mOrigin;
    private LatLng mDestination;
    private Polyline mPolyline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);


//        String lat=getIntent().getStringExtra("lat");
//        String lng=getIntent().getStringExtra("lng");


        String lat= Common.currentResult.getGeometry().getLocation().getLat();
        String lng=Common.currentResult.getGeometry().getLocation().getLng();
        LatLng latLng=new LatLng(Double.parseDouble(lat),Double.parseDouble(lng));
        mDestination = latLng;


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getMyLocation();
        mMap.clear();
        mMarkerOptions = new MarkerOptions().position(mDestination).title("Destination");
        mMap.addMarker(mMarkerOptions);
        if(mOrigin != null && mDestination != null)
            drawRoute();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

        if (requestCode == 100){
            if (!verifyAllPermissions(grantResults)) {
                Toast.makeText(getApplicationContext(),"No sufficient permissions",Toast.LENGTH_LONG).show();
            }else{
                getMyLocation();
            }
        }else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean verifyAllPermissions(int[] grantResults) {

        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    private void getMyLocation(){

        // Getting LocationManager object from System Service LOCATION_SERVICE
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mOrigin = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mOrigin,12));
                if(mOrigin != null && mDestination != null)
                drawRoute();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        int currentApiVersion = Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_DENIED) {
                mMap.setMyLocationEnabled(true);
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,10000,0,mLocationListener);

//                mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
//                    @Override
//                    public void onMapLongClick(LatLng latLng) {
//                        mDestination = latLng;
//                        mMap.clear();
//                        mMarkerOptions = new MarkerOptions().position(mDestination).title("Destination");
//                        mMap.addMarker(mMarkerOptions);
//                        if(mOrigin != null && mDestination != null)
//                        drawRoute();
//                    }
//                });

            }else{
                requestPermissions(new String[]{
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                },100);
            }
        }
    }


    private void drawRoute(){

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(mOrigin, mDestination);

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }


    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Key
        String key = "key=" + getString(R.string.google_maps_key);

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+key;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception on download", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /** A class to download data from Google Directions URL */
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("DownloadTask","DownloadTask : " + data);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Directions in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionJSONParser parser = new DirectionJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                if(mPolyline != null){
                    mPolyline.remove();
                }
                mPolyline = mMap.addPolyline(lineOptions);

            }else
                Toast.makeText(getApplicationContext(),"No route is found", Toast.LENGTH_LONG).show();
        }
    }

}
//import android.Manifest;
//import android.app.AlertDialog;
//import android.content.pm.PackageManager;
//import android.graphics.Camera;
//import android.graphics.Color;
//import android.location.Location;
//import android.os.AsyncTask;
//import android.os.Looper;
//import android.support.v4.app.ActivityCompat;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//
//import com.example.skixpert_trackingskiperformance.HelperDirections.DirectionJSONParser;
//import com.example.skixpert_trackingskiperformance.Retrofit.Common;
//import com.example.skixpert_trackingskiperformance.Retrofit.IGoogleApiService;
//import com.google.android.gms.location.FusedLocationProviderClient;
//import com.google.android.gms.location.LocationCallback;
//import com.google.android.gms.location.LocationRequest;
//import com.google.android.gms.location.LocationResult;
//import com.google.android.gms.location.LocationServices;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.android.gms.maps.model.PolygonOptions;
//import com.google.android.gms.maps.model.Polyline;
//import com.google.android.gms.maps.model.PolylineOptions;
//import com.google.android.gms.tasks.OnSuccessListener;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//
//import dmax.dialog.SpotsDialog;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class ViewDirections extends FragmentActivity implements OnMapReadyCallback {
//
//    private GoogleMap mMap;
//    FusedLocationProviderClient fusedLocationProviderClient;
//    LocationCallback locationCallback;
//    LocationRequest locationRequest;
//    Location mLastLocation;
//    Marker mMarker;
//    Polyline polyline;
//    IGoogleApiService mService;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_view_directions);
//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//
//        mService=Common.getGoogleApiServiceScalars();
//
//        buildLocationRequest();
//        buildLocationCallback();
//        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//
//            return;
//        }
//        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
//    }
//
//
//    @Override
//    protected void onStop() {
//        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
//        super.onStop();
//    }
//
//    private void buildLocationRequest() {
//        locationRequest = new LocationRequest();
//        locationRequest = new LocationRequest();
//        locationRequest.setInterval(1000);
//        locationRequest.setFastestInterval(1000);
//        locationRequest.setSmallestDisplacement(10f);
//        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
//
//    }
//
//    private void buildLocationCallback() {
//        locationCallback = new LocationCallback() {
//            @Override
//            public void onLocationResult(LocationResult locationResult) {
//                super.onLocationResult(locationResult);
//                mLastLocation = locationResult.getLastLocation();
//
//
//                MarkerOptions markerOptions = new MarkerOptions()
//                        .position(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()))
//                        .title("Your position")
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//                mMarker = mMap.addMarker(markerOptions);
//
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude())));
//                mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
//
//                LatLng destinationLatLng=new LatLng(Double.parseDouble(Common.currentResult.getGeometry().getLocation().getLat()),
//                        Double.parseDouble( Common.currentResult.getGeometry().getLocation().getLng()));
//
//
//                mMap.addMarker( new MarkerOptions()
//                        .position(destinationLatLng)
//                        .title(Common.currentResult.getName())
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
//
//                drawPath(mLastLocation,Common.currentResult.getGeometry().getLocation());
//
//            }
//        };
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        mMap.getUiSettings().setZoomControlsEnabled(true);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//           return;
//        }
//        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
//            @Override
//            public void onSuccess(Location location) {
//            mLastLocation=location;
//
//                MarkerOptions markerOptions = new MarkerOptions()
//                        .position(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()))
//                        .title("Your position")
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//                mMarker = mMap.addMarker(markerOptions);
//
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude())));
//                mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
//
//            LatLng destinationLatLng=new LatLng(Double.parseDouble(Common.currentResult.getGeometry().getLocation().getLat()),
//                   Double.parseDouble( Common.currentResult.getGeometry().getLocation().getLng()));
//
//
//                 mMap.addMarker( new MarkerOptions()
//                         .position(destinationLatLng)
//                         .title(Common.currentResult.getName())
//                         .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
//
//                 drawPath(mLastLocation,Common.currentResult.getGeometry().getLocation());
//
//            }
//
//
//        });
//    }
//
//    private void drawPath(Location mLastLocation, com.example.skixpert_trackingskiperformance.Model.Location location) {
//
//   if(polyline !=null)
//       polyline.remove();
//
//
//   String origin=new StringBuilder(String.valueOf(mLastLocation.getLatitude())).append(",").append(String.valueOf(mLastLocation.getLongitude()))
//           .toString();
//
//   String destination=new StringBuilder(location.getLat()).append(",").append(location.getLng()).toString();
//
//   String key= getResources().getString(R.string.google_maps_key);
//
//
//
//   mService.getDirections(origin,destination)
//           .enqueue(new Callback<String>() {
//               @Override
//               public void onResponse(Call<String> call, Response<String> response) {
//                   new ParserTask().execute(response.body().toString());
//               }
//
//               @Override
//               public void onFailure(Call<String> call, Throwable t) {
//
//               }
//           });
//
//    }
//
//    private class ParserTask extends AsyncTask<String,Integer,List<List<HashMap<String,String>>>> {
//
//        AlertDialog waitingDialog=new SpotsDialog((ViewDirections.this));
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            waitingDialog.show();
//            waitingDialog.setMessage("Please wait");
//        }
//
//        @Override
//        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
//            JSONObject jsonObject;
//            List<List<HashMap<String, String>>> routes=null;
//            try{
//                jsonObject=new JSONObject(strings[0]);
//                DirectionJSONParser parser=new DirectionJSONParser();
//                routes= parser.parse(jsonObject);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return routes;
//        }
//
//        @Override
//        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
//            super.onPostExecute(lists);
//
//            waitingDialog.dismiss();
//
//            ArrayList points=null;
//            PolylineOptions polylineOptions=null;
//            for(int i=0;i<lists.size();i++)
//            {
//                points=new ArrayList();
//                polylineOptions=new PolylineOptions();
//
//                List<HashMap<String,String>> path=lists.get(i);
//                for(int j=0;j<path.size();j++)
//                {
//                    HashMap<String,String> point=path.get(j);
//                    double lat=Double.parseDouble(point.get("lat"));
//                    double lng=Double.parseDouble(point.get("lng"));
//
//                    LatLng position=new LatLng(lat,lng);
//                    points.add(position);
//                }
//
//                polylineOptions.addAll(points);
//                polylineOptions.width(12);
//                polylineOptions.color(Color.RED);
//                polylineOptions.geodesic(true);
//            }
//            if(polylineOptions!=null)
//            polyline=mMap.addPolyline(polylineOptions);
//        }
//    }
//}
//



