package com.example.skixpert_trackingskiperformance;

import android.location.Location;

public class CLocation extends Location {

    private boolean bUseMetricUnits=false;

    public CLocation(Location location){
        this(location,true);
    }

    public CLocation(Location location, boolean bUseMetricUnits){
        super(location);
        this.bUseMetricUnits=bUseMetricUnits;
    }

    public boolean getUseMtricUnits(){
        return this.bUseMetricUnits;
    }

    public void setUserMetricUnits(boolean bUseMetricUnits){
        this.bUseMetricUnits=bUseMetricUnits;
    }

    @Override
    public float distanceTo(Location dest) {
        float nDistance=super.distanceTo(dest);
        if (!this.getUseMtricUnits()){
            nDistance=nDistance *3.2808399f;
        }
        return nDistance;
    }

    @Override
    public double getAltitude() {
        double nAltitude=super.getAltitude();
        if (!this.getUseMtricUnits()){
            nAltitude=nAltitude *3.2808399d;
        }
        return nAltitude;
    }

    @Override
    public float getSpeed() {
        float nSpeed=super.getSpeed() * 3.6f;

        if (!this.getUseMtricUnits()){
            //convert meters/second to miles/hour
            nSpeed=super.getSpeed() *2.23693629f;
        }

        return nSpeed;     }



    @Override
    public float getAccuracy() {

        float nAccuracy=super.getAccuracy();

        if (!this.getUseMtricUnits()){
            //convert meters to feet
            nAccuracy=nAccuracy *3.2808399f;
        }

        return nAccuracy;     }

    @Override
    public double getLatitude() {
        return super.getLatitude();
    }

    @Override
    public double getLongitude() {
        return super.getLongitude();
    }
}
