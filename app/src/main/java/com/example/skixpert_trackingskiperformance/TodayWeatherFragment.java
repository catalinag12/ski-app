package com.example.skixpert_trackingskiperformance;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.skixpert_trackingskiperformance.Common.CommonWeather;
import com.example.skixpert_trackingskiperformance.Model_weather.WeatherResult;

import com.example.skixpert_trackingskiperformance.Retrofit.IOpenWeatherMap;
import com.example.skixpert_trackingskiperformance.Retrofit.RetrofitClient;
import com.squareup.picasso.Picasso;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;


/**
 * A simple {@link Fragment} subclass.
 */
public class TodayWeatherFragment extends Fragment {


    ImageView img_weather;
    TextView txt_city, txt_humidity, txt_pressure,txt_temperature, txt_sunset, txt_sunrise,txt_wind,txt_geo_coords, txt_description, txt_date_time;
    LinearLayout weather_panel;
    ProgressBar loading;
    CompositeDisposable compositeDisposable;
    IOpenWeatherMap mService;

    static TodayWeatherFragment instance;

    public static TodayWeatherFragment getInstance(){
        if(instance==null)
            instance=new TodayWeatherFragment();
        return instance;
    }

    public TodayWeatherFragment() {
        // Required empty public constructor
        compositeDisposable=new CompositeDisposable();
        Retrofit retrofit= RetrofitClient.getInstance();
        mService=retrofit.create(IOpenWeatherMap.class);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View itemview= inflater.inflate(R.layout.fragment_today_weather, container, false);
    img_weather=itemview.findViewById(R.id.imgWeather);
    txt_city=itemview.findViewById(R.id.tvCityName);
        txt_pressure=itemview.findViewById(R.id.tv_pressure);
        txt_humidity=itemview.findViewById(R.id.tv_humidity);
        txt_wind=itemview.findViewById(R.id.tv_wind);
        txt_sunrise=itemview.findViewById(R.id.tvSunrise);
        txt_sunset=itemview.findViewById(R.id.tvSunset);
        txt_geo_coords=itemview.findViewById(R.id.tvGeoCoords);
        txt_date_time=itemview.findViewById(R.id.tvDateTime);
        txt_description=itemview.findViewById(R.id.tv_description);
        txt_temperature=itemview.findViewById(R.id.tv_temperature);

        weather_panel=itemview.findViewById(R.id.weather_panel);
        loading=itemview.findViewById(R.id.loading);
        getWeatherInformation();



        return itemview;
    }

    private void getWeatherInformation() {
        compositeDisposable.add(mService.getWeatherByLatLng(String.valueOf(CommonWeather.current_location.getLatitude()),
                String.valueOf(CommonWeather.current_location.getLongitude()),
                        CommonWeather.APP_ID,
                        "metric").subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<WeatherResult>() {
            @Override
            public void accept(WeatherResult weatherResult) throws Exception {
                //load image
                Picasso.get().load(new StringBuilder("https://openweathermap.org/img/w/")
                        .append(weatherResult.getWeather().get(0).getIcon())
                .append(".png").toString()).into(img_weather);

                txt_city.setText(weatherResult.getName());
                txt_description.setText(new StringBuilder("Weather in:").
                        append(weatherResult.getName()).toString());
                txt_temperature.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getTemp())).append("C").toString());
                txt_date_time.setText(CommonWeather.convertUnixToDate(weatherResult.getDt()));
                txt_pressure.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getPressure())).append(" hpa").toString());
                txt_humidity.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getHumidity())).append("%"));
                txt_sunrise.setText(CommonWeather.convertUnixToHour(weatherResult.getSys().getSunrise()));
                txt_sunset.setText(CommonWeather.convertUnixToHour(weatherResult.getSys().getSunset()));
                txt_geo_coords.setText(new StringBuilder("(").append(weatherResult.getCoord().toString()));
                txt_wind.setText(new StringBuilder(String.valueOf(weatherResult.getWind().getSpeed())));
            //display panel

                weather_panel.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {

                Toast.makeText(getActivity(),throwable.getMessage(),Toast.LENGTH_LONG).show();
            }
        }));
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    public void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }
}
