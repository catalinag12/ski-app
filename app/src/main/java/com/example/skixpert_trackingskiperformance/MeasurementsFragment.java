package com.example.skixpert_trackingskiperformance;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.GoogleApiClient;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Formatter;
import java.util.Locale;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.BIND_AUTO_CREATE;
import static android.content.Context.LOCATION_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */

public class MeasurementsFragment extends Fragment implements View.OnClickListener, EditorView, LocationListener, GoogleApiClient.ConnectionCallbacks {
    private static final String TAG = "MeasurementsFragment";
    SwitchCompat sw_units;
    public static final String PREFS_NAME = "MyPrefsFile";

    public static boolean metrics=false;


    TextView  tvAltitude, tvMaxSpeed, tvAvgSpeed, tvMaxAlt,tvMinAlt,tvSecondsMinutes,tvMeasurementSpeed,tvMeasurementAltitude;
    ImageView buttonPauseResume;
    TextView tvSpeed;
    private long pauseOffset=0;

    boolean dialogShown=false,dialogShown1=false;
    long timeWhenStopped=0;
    TextView tvDate;
    static double angle;

    private BottomSheetDialog bottomSheetDialog;

    LocationManager locationManager;
    static TextView tvDistance;
    static TextView tvTime;
    Button btnStart, btnEnd, btnPause;
    ImageView buttonStop;

    int dur,user_id;


    TextView tvInclination;
    TextView tvRating, tvDescription;
    static boolean status;
    static long startTime, endTime, startPauseTime, endPauseTime;
    static ProgressDialog progressDialog,pbWait;
    public static int p = 0;

    public Chronometer chronometer;
    public static boolean running;

    public static int s,slope;
    Button btnSave;
    Button btnRetrofit;

    GoogleApiClient mGoogleApiClient;

    ApiInterface apiInterface;

    EditorPresenter presenter;


    ArrayList<Float> speedArray = new ArrayList();
    ArrayList<Double> altitudeArray=new ArrayList<>();

    ArrayList<Float> speedArray1 = new ArrayList();
    ArrayList<Double> altitudeArray1=new ArrayList<>();
    LocationService myService;

    BottomNavigationView bottomNavigationView;


    private ServiceConnection sc = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder iBinder) {

            LocationService.LocalBinder binder = (LocationService.LocalBinder) iBinder;
            myService = binder.getService();
            status = true;

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

            status = false;
        }
    };


    @Override
    public void onDestroy() {
        if (status == true)
            unbindService();
        super.onDestroy();
    }

    private void unbindService() {
        if (status == false)
            return;
        Intent i = new Intent(getActivity().getApplicationContext(), LocationService.class);
        getActivity().unbindService(sc);
        status = false;

    }

    @Override
    public void onDetach() {
        if (status == false) {
            super.onDetach();
        } else {
            getActivity().moveTaskToBack(true);
        }
    }


    public MeasurementsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View myFragmentView = inflater.inflate(R.layout.fragment_measurements, container, false);

        tvSpeed = myFragmentView.findViewById(R.id.tvSpeed);
        sw_units = myFragmentView.findViewById(R.id.switch1);
        tvAltitude = myFragmentView.findViewById(R.id.tvAltitude);
        tvMaxAlt=myFragmentView.findViewById(R.id.tvMaxAlt);
        tvMinAlt=myFragmentView.findViewById(R.id.tvMinAlt);

        startTime = System.currentTimeMillis();

        chronometer=myFragmentView.findViewById(R.id.chronometer);



        tvMeasurementAltitude=myFragmentView.findViewById(R.id.tvMeasurementAltitude);
        tvMeasurementSpeed=myFragmentView.findViewById(R.id.tvMeasurementSpeed);




        sw_units.setChecked(true);
//       final SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
//
//         SharedPreferences.Editor editor=settings.edit();

        tvInclination=myFragmentView.findViewById(R.id.tvInclination);

        tvMaxSpeed = myFragmentView.findViewById(R.id.tvMaxSpeed);
        tvAvgSpeed = myFragmentView.findViewById(R.id.tvAvgSpeed);

        tvDate=myFragmentView.findViewById(R.id.tvDateNow);

        tvRating=myFragmentView.findViewById(R.id.tvRate);
        tvDescription=myFragmentView.findViewById(R.id.tvDescription);


        //btnSave=myFragmentView.findViewById(R.id.btnSave);
        buttonStop=myFragmentView.findViewById(R.id.buttonStop);
        buttonPauseResume=myFragmentView.findViewById(R.id.buttonPauseResume);


        tvDistance = myFragmentView.findViewById(R.id.tvDistance);
        tvTime = myFragmentView.findViewById(R.id.tvTime);



        pbWait=new ProgressDialog(getContext());
        pbWait.setMessage("Please wait...");
        tvSecondsMinutes=myFragmentView.findViewById(R.id.tvSecondsMinutes);


        startChronometer();
        bottomSheetDialog=new BottomSheetDialog(getContext());
        View bottomSheetDialogView=getLayoutInflater().inflate(R.layout.dialog_layout1,null);

        View savingView=bottomSheetDialogView.findViewById(R.id.saving);
        View deletingView=bottomSheetDialogView.findViewById(R.id.deleting);
        View sharingView=bottomSheetDialogView.findViewById(R.id.sharing);

        buttonStop.setOnClickListener(this::onClick);
        savingView.setOnClickListener(this::onClick);
        deletingView.setOnClickListener(this::onClick);
        sharingView.setOnClickListener(this::onClick);



        bottomSheetDialog.setContentView(bottomSheetDialogView);

        presenter=new EditorPresenter(this);

        Calendar calendar=Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy / MM / dd ");
        String strDate = mdformat.format(calendar.getTime());
        String description=strDate;

        tvDate.setText(description);
        tvRating.setText("0");



        //Toast.makeText(getContext(),tvDate.getText().toString(),Toast.LENGTH_LONG).show();









        bottomNavigationView = myFragmentView.findViewById(R.id.bottom_navigation);
        //check for gps permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1000);
        } else {
            //start the program if the permission is granted
            doStuff();
        }




        sw_units.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MeasurementsFragment.this.updateSpeed(null);
                if(!isChecked) metrics=true;
                else{metrics=false;}
            }
        });


        //request permission
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {

        }

        requestPermissions(new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        }, 1000);


        startMeasurements();


        getActivity().registerReceiver(bcr,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {


                    case R.id.nav_diary:
                        Intent intent = new Intent(getContext(), DiaryActivity.class);
                        startActivity(intent);
                        break;


                    case R.id.nav_map:
                        Intent intent2 = new Intent(getContext(), MapActivity.class);
                        startActivity(intent2);
                        break;

                    case R.id.nav_statistics:
                        Intent intent3 = new Intent(getContext(), SettingsActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.nav_resorts:
                        Intent intent4 = new Intent(getContext(), ResortActivity.class);
                        startActivity(intent4);
                        break;

                    case R.id.nav_home:
                        break;
                }
                return false;
            }
        });

        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long time = SystemClock.elapsedRealtime() - chronometer.getBase();
                int h   = (int)(time /3600000);
                int m = (int)(time - h*3600000)/60000;
                int s= (int)(time - h*3600000- m*60000)/1000 ;
                String t = (h < 10 ? "0"+h: h)+":"+(m < 10 ? "0"+m: m)+":"+ (s < 10 ? "0"+s: s);
                chronometer.setText(t);
            }
        });

        buttonPauseResume.setTag("pause");

        buttonPauseResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(buttonPauseResume.getTag().equals("pause")){
                    buttonPauseResume.setImageResource(R.drawable.ic_starttt);
                    buttonPauseResume.setTag("resume");
                    p=1;
                    pauseChronometer();

                }
                else if(buttonPauseResume.getTag().equals("resume")){
                    buttonPauseResume.setTag("pause");
                    buttonPauseResume.setImageResource(R.drawable.ic_pause);
                    checkGPS();
                    locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                        return;
                    p=0;
                    startChronometer();
                }

            }
        });





        return myFragmentView;

    }



    private void checkGPS() {
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            showGPSDisabledAlert();
    }

    private void showGPSDisabledAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage("Enable GPS to use application")
                .setCancelable(false)
                .setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void bindService() {
        if (status == true)
            return;
        Intent i = new Intent(getContext(), LocationService.class);
        getActivity().bindService(i, sc, BIND_AUTO_CREATE);
        status = true;
        // startTime = System.currentTimeMillis();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            CLocation myLocation = new CLocation(location, this.useMetricUnits());
            this.updateSpeed(myLocation);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    @SuppressLint("MissingPermission")
    private void doStuff() {
        LocationManager locationManager = (LocationManager) this.getActivity().getSystemService(LOCATION_SERVICE);
        if (locationManager != null) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
        Toast.makeText(getActivity(), "Waiting for GPS connection", Toast.LENGTH_LONG).show();

    }



    private void updateSpeed(CLocation location) {
        //final SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        //dialogShown = settings.getBoolean("dialogShown", false);
        float nCurrentSpeed=0;
        float avg=0;
        double nAlt=0;
        double minAlt=0,maxAlt=0;
        if(location!=null && p==0){
            location.setUserMetricUnits(this.useMetricUnits());
            nCurrentSpeed=location.getSpeed();
            nAlt=location.getAltitude();

            if(!speedArray.contains(nCurrentSpeed) && this.useMetricUnits()) {
                metrics=false;
                speedArray.add(nCurrentSpeed); }

            if(!speedArray1.contains(nCurrentSpeed) && !this.useMetricUnits()) {
                metrics=true;
                speedArray1.add(nCurrentSpeed); }

            if(this.useMetricUnits()){
                tvMaxSpeed.setText(new DecimalFormat("0.000").format(Collections.max(speedArray)));
                metrics=false;}
            else{
                tvMaxSpeed.setText(new DecimalFormat("0.000").format(Collections.max(speedArray1)));
                metrics=true;}

            if(this.useMetricUnits()) {
                metrics=false;
                float sum = 0;
                for (float val : speedArray) {
                    if (val != 0)
                        sum += val;
                }
                avg = sum / (speedArray.size());
                tvAvgSpeed.setText(new DecimalFormat("0.000").format(avg));

            }

            if(!this.useMetricUnits()) {
                metrics=true;
                float sum = 0;
                for (float val : speedArray1) {
                    if (val != 0)
                        sum += val;
                }
                avg = sum / (speedArray1.size());
                tvAvgSpeed.setText(new DecimalFormat("0.000").format(avg));

            }
            if(!altitudeArray.contains(nAlt) && this.useMetricUnits()){
                metrics=false;
                altitudeArray.add(nAlt);
            }

            if(!altitudeArray.contains(nAlt) && !this.useMetricUnits()){
                metrics=true;
                altitudeArray1.add(nAlt);
            }

            if(this.useMetricUnits()) {
                metrics=false;
                tvMinAlt.setText(new DecimalFormat("0.0").format(Collections.min(altitudeArray)));
                tvMaxAlt.setText(new DecimalFormat("0.0").format(Collections.max(altitudeArray)));
            }
            if(!this.useMetricUnits()) {
                metrics=true;
                tvMinAlt.setText(new DecimalFormat("0.0").format(Collections.min(altitudeArray1)));
                tvMaxAlt.setText(new DecimalFormat("0.0").format(Collections.max(altitudeArray1)));
            }

            if (Double.parseDouble(tvDistance.getText().toString())!=0){
                Double tangent=Math.tan(Math.toRadians((Double.parseDouble(tvDistance.getText().toString().trim()))*1000/Double.parseDouble(tvAltitude.getText().toString().trim())));
                angle=Float.parseFloat(String.valueOf(Math.toDegrees(tangent)));
            }
            else angle=1;
            tvInclination.setText(new DecimalFormat("0.0000").format(angle));

            s=SettingsActivity.speed;

            float sp = Float.parseFloat(tvSpeed.getText().toString());
            // Toast.makeText(getContext(),"SPEEED" + s,Toast.LENGTH_LONG).show();

            if(sp>SettingsActivity.speed) {
                if(!dialogShown){
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
                    alertBuilder.setTitle("Exceeded speed ");
                    alertBuilder.setMessage("You need to slow down!");
                    alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }

                        ;
                    });
                    AlertDialog ad = alertBuilder.create();
                    ad.show();
                    dialogShown=true;
                }}
           // if(Double.parseDouble(tvDistance.getText().toString().trim())!=0) {
             //   float slopee = Float.parseFloat(tvInclination.getText().toString());

//                if (slopee > SettingsActivity.slope) {
//                    if(!dialogShown){
//                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
//                        alertBuilder.setTitle("Danger ");
//                        alertBuilder.setMessage("The location is prone to avalanches!");
//                        alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//
//                            ;
//                        });
                      //  AlertDialog ad = alertBuilder.create();
                       // ad.show();
                        //dialogShown=true;

                        // SharedPreferences.Editor editor=settings.edit();
                        //editor.putBoolean("dialogShown",true);
                        //editor.commit();



//                    }}
//            }
      }


        Formatter fmt=new Formatter(new StringBuilder());
        fmt.format(Locale.US, "%3.1f",nCurrentSpeed);

        String strCurrentSpeed=fmt.toString();
        strCurrentSpeed=strCurrentSpeed.replace(" ", "0");

        Formatter fmt2=new Formatter(new StringBuilder());

        fmt2.format(Locale.US,"%4.1f",nAlt);
        String strAlt=fmt2.toString();
        if(this.useMetricUnits()) {
            tvAltitude.setText(strAlt);
            tvMeasurementAltitude.setText("meters");
        }
        else{
            tvAltitude.setText(strAlt);
            tvMeasurementAltitude.setText("feet");

        }
        if(this.useMetricUnits()){
            tvSpeed.setText(strCurrentSpeed ); //adauga km / h
            tvMeasurementSpeed.setText("km/h");
        }
        else{
            tvSpeed.setText(strCurrentSpeed ); //miles
            tvMeasurementSpeed.setText("miles/h");
        }
        //computeAngle();

        //Toast.makeText(getContext(),"The slope has : " + angle  + " degrees ",Toast.LENGTH_LONG).show();



    }

    public void startMeasurements(){
        checkGPS();
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            return;
        if (status == false)
            bindService();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Getting location...");
        progressDialog.show();


//        Toast.makeText(getContext(),"SPEED LEVEL: "  +SettingsActivity.speed,Toast.LENGTH_LONG).show();
        updateSpeed(null);

    }
    private boolean useMetricUnits(){
        return sw_units.isChecked();

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==1000){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getContext(),"GRANTED",Toast.LENGTH_LONG).show();
                doStuff();
            }else{
                Toast.makeText(getContext(), "DENIED", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }


    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void showProgress() {
        pbWait.show();
    }

    @Override
    public void hideProgress() {
        pbWait.hide();
    }

    @Override
    public void onRequestSuccess(String message) {

        Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show();
        getActivity().finish();
    }

    @Override
    public void onRequestError(String message) {
        Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show();
        getActivity().finish();
    }


    public void startChronometer(){
        if(!running){
            chronometer.setBase(SystemClock.elapsedRealtime()-pauseOffset);
            chronometer.start();
            running=true;

            long timeElapsed = SystemClock.elapsedRealtime() - chronometer.getBase();
            int hours = (int) (timeElapsed / 3600000);
            int minutes = (int) (timeElapsed - hours * 3600000) / 60000;
            int seconds = (int) (timeElapsed - hours * 3600000 - minutes * 60000) / 1000;

            if (hours != 0) {
                hours=hours*60 +minutes;
                tvTime.setText(String.valueOf(hours));
                dur=hours;
                String m=" minutes";
                tvSecondsMinutes.setText(String.valueOf(m));
            }

            if (minutes != 0) {
                tvTime.setText(String.valueOf(minutes));
                dur=minutes;

                String m=" minutes";
                tvSecondsMinutes.setText(String.valueOf(m));
            }
            else if(hours==0 && minutes==0) {
                dur=0;
                tvTime.setText(String.valueOf(seconds) );
                String m=" seconds";
                tvSecondsMinutes.setText(String.valueOf(m));
            }




        }
    }

    public void pauseChronometer(){
        if(running){
            chronometer.stop();
            pauseOffset=SystemClock.elapsedRealtime()-chronometer.getBase();
            running=false;
        }



    }


    private BroadcastReceiver bcr=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int level=intent.getIntExtra("level",0);
            if(level<SettingsActivity.battery){
                AlertDialog.Builder alertBuilder=new AlertDialog.Builder(getContext());
                alertBuilder.setTitle("Low battery");
                alertBuilder.setMessage("You need to charge your phone!");
                alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    };
                });
                AlertDialog ad=alertBuilder.create();
                ad.show();
            }
        }
    };


    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch(id){
            case R.id.buttonStop:
                bottomSheetDialog.show();
                break;


            case R.id.sharing:

                String max_message ="I reached " + tvMaxSpeed.getText().toString()+ " kms!!";
                Intent sendIntent=new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,max_message);
                sendIntent.setType("text/plain");
                Intent shareIntent=Intent.createChooser(sendIntent,null);
                startActivity(shareIntent);
                break;
            case R.id.saving:


                int value=HomePageActivity.id_user_good;

                //Toast.makeText(getContext(),u,Toast.LENGTH_LONG).show();
                float max_speed = Float.parseFloat(tvMaxSpeed.getText().toString());
                float avg_speed = Float.parseFloat(tvAvgSpeed.getText().toString());
                double min_altitude = Double.parseDouble(tvMinAlt.getText().toString());
                double max_altitude = Double.parseDouble(tvMaxAlt.getText().toString());
                double distance = Double.parseDouble(tvDistance.getText().toString());
                int duration = Integer.parseInt(tvTime.getText().toString());
                int rating=Integer.parseInt(tvRating.getText().toString());
                String description=tvDate.getText().toString();

                presenter.saveToDB(value,max_speed,avg_speed,max_altitude,min_altitude,distance,dur,rating,description);
                startActivity(new Intent(getContext(),DiaryActivity.class));
                break;
            case R.id.deleting:
                startActivity(new Intent(getContext(),HomePageActivity.class));
                break;


        }
    }
};






