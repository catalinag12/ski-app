package com.example.skixpert_trackingskiperformance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

public class Measurements {

    @Expose
    @SerializedName("measurement_id") private int id;
    @Expose
    @SerializedName("user_id") private int user_id;
    @Expose
    @SerializedName("max_speed") private float max_speed;
    @Expose
    @SerializedName("avg_speed") private float avg_speed;
    @Expose
    @SerializedName("max_altitude") private double max_altitude;
    @Expose
    @SerializedName("min_altitude") private double min_altitude;
    @Expose
    @SerializedName("distance") private double distance;
    @Expose
    @SerializedName("duration") private int duration;
    @Expose
    @SerializedName("rating") private int rating;

    @Expose
    @SerializedName("description") private String  description;

    @Expose
    @SerializedName("success") private Boolean success;

    @Expose
    @SerializedName("message") private String message;




    public int getMeasurementId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public float getMax_speed() {
        return max_speed;
    }

    public void setMax_speed(float max_speed) {
        this.max_speed = max_speed;
    }

    public float getAvg_speed() {
        return avg_speed;
    }

    public void setAvg_speed(float avg_speed) {
        this.avg_speed = avg_speed;
    }

    public double getMax_altitude() {
        return max_altitude;
    }

    public void setMax_altitude(double max_altitude) {
        this.max_altitude = max_altitude;
    }

    public double getMin_altitude() {
        return min_altitude;
    }

    public void setMin_altitude(double min_altitude) {
        this.min_altitude = min_altitude;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static final Comparator<Measurements>  BY_RATING_ASCENDING=new Comparator<Measurements>() {
        @Override
        public int compare(Measurements o1, Measurements o2) {
            return ((Integer)o1.getRating()).compareTo(o2.getRating());
        }
    };

    public static final Comparator<Measurements>  BY_RATING_DESCENDING=new Comparator<Measurements>() {
        @Override
        public int compare(Measurements o1, Measurements o2) {
            return ((Integer)o2.getRating()).compareTo(o1.getRating());
        }
    };
    public static final Comparator<Measurements>  BY_DISTANCE_ASCENDING=new Comparator<Measurements>() {
        @Override
        public int compare(Measurements o1, Measurements o2) {
            return ((Double)o1.getDistance()).compareTo(o2.getDistance());
        }
    };

    public static final Comparator<Measurements>  BY_DISTANCE_DESCENDING=new Comparator<Measurements>() {
        @Override
        public int compare(Measurements o1, Measurements o2) {
            return ((Double)o2.getDistance()).compareTo(o1.getDistance());
        }
    };
    public static final Comparator<Measurements>  BY_DATE_ASCENDING=new Comparator<Measurements>() {
        @Override
        public int compare(Measurements o1, Measurements o2) {
            return ((Integer)o1.getMeasurementId()).compareTo(o2.getMeasurementId());
        }
    };

    public static final Comparator<Measurements>  BY_DATE_DESCENDING=new Comparator<Measurements>() {
        @Override
        public int compare(Measurements o1, Measurements o2) {
            return ((Integer)o2.getMeasurementId()).compareTo(o1.getMeasurementId());
        }
    };

}
