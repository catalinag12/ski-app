package com.example.skixpert_trackingskiperformance;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Map;
import java.util.Set;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {


    public FirebaseMessagingService() {
        super();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Map<String, String> data = remoteMessage.getData();
        String messageStr = data.get("message");
        String deepLink = data.get("adb_deeplink");

        sendNotification(deepLink, messageStr, data);
    }

    private void sendNotification(String deeplink, String message, Map<String, String> data) {
        Intent intent;

        if (deeplink != null) {
            intent = new Intent(this, HomePageActivity.class);
        } else {
            intent = new Intent(this, HomePageActivity.class);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Bundle pushData = new Bundle();
        Set<String> keySet = data.keySet();
        for (String key : keySet) {
            pushData.putString(key, data.get(key));
        }

        intent.putExtras(pushData);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon_star)
                .setContentTitle("FCM Deep Link Push")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }
}