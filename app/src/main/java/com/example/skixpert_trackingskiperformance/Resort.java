package com.example.skixpert_trackingskiperformance;

public class Resort {


    private int id;
    private String location;
    private float latitude;
    private float longitude;

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    private String program;
    private String description;
    private String prices;
    private String  rating;
    private String image;


    public Resort(int id, String location,float latitude,float longitude, String program, String description, String prices, String rating, String image) {
        this.id = id;
        this.location = location;
        this.latitude=latitude;
        this.longitude=longitude;
        this.program = program;
        this.description = description;
        this.prices = prices;
        this.rating = rating;
        this.image = image;
    }




    public Resort(int id, String location, String rating , String image) {
        this.id = id;
        this.location = location;
        this.rating = rating;
        this.image = image;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrices() {
        return prices;
    }

    public void setPrices(String prices) {
        this.prices = prices;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
