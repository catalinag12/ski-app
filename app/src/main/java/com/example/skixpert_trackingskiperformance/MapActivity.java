package com.example.skixpert_trackingskiperformance;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;
import java.text.DecimalFormat;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener,
        ResortAdapter.OnItemClickListener{

    private static final String RESORT_URL="http://192.168.0.178/licenta/resorts.php";
    ArrayList<Resort> resortList;
   public static LatLng locatiecurenta;
    RecyclerView recyclerView;
    ResortAdapter resortAdapter;
    TextView tvTemp;
    ImageView imgState;
    private double distante[];
    private GoogleMap mMap;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    ZoomControls zoom;
    Button btnMark;
    float myLatitude = 0;
    float myLongitude = 0;
    Button btnGetLocation;
    Button satelliteView;

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;

    protected static final String TAG = "MapActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        resortList=new ArrayList<>();
        recyclerView=findViewById(R.id.recyclerViewResorts);
        recyclerView.setHasFixedSize(true);

        //by default vertical
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        locatie();






        tvTemp=findViewById(R.id.tvTemp);
        imgState=findViewById(R.id.imgState);

        btnMark = findViewById(R.id.btMark);
        btnGetLocation=findViewById(R.id.btSearch);
        satelliteView=findViewById(R.id.btSatellite);
        satelliteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mMap.getMapType()== GoogleMap.MAP_TYPE_NORMAL){
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    satelliteView.setText("NORMAL");
                }
                else{
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    satelliteView.setText("SATELLITE");
                }
            }
        });

        btnGetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText searchText = findViewById(R.id.etLocationEntry);
                String search = searchText.getText().toString();
                if (search != null && !search.equals("")) {
                    List<Address> addressList = null;
                    Geocoder geocoder = new Geocoder(MapActivity.this);
                    try {
                        addressList = geocoder.getFromLocationName(search, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Address address = addressList.get(0);
                    LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(latLng).title("from geocoder"));
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                }
            }
        });


        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        locationRequest = new LocationRequest();
        locationRequest.setInterval(15 * 100);
        locationRequest.setFastestInterval(5 * 1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //set to balanced power accuracy on real device

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        Intent intentHome = new Intent(getApplicationContext(), HomePageActivity.class);
                        startActivity(intentHome);

                        break;

                    case R.id.nav_diary:
                        Intent intent = new Intent(getApplicationContext(), DiaryActivity.class);
                        startActivity(intent);
                        break;


                    case R.id.nav_map:
                        Intent intent2 = new Intent(getApplicationContext(), MapActivity.class);
                        startActivity(intent2);
                        break;

                    case R.id.nav_statistics:
                        Intent intent3 = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.nav_resorts:
                        Intent intent4 = new Intent(getApplicationContext(), ResortActivity.class);
                        startActivity(intent4);
                        break;

                }
                return false;
            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        zoom = findViewById(R.id.zcZoom);
        zoom.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.animateCamera(CameraUpdateFactory.zoomOut());
            }
        });
        zoom.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
            }
        });

        btnMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng myLocation = new LatLng(myLatitude, myLongitude);
                mMap.addMarker(new MarkerOptions().position(myLocation).title("My Location"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation,16.0f));
            }
        });


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        addMarkerOnMap(mMap,resortList);

        //need permission to fine location
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mMap.setMyLocationEnabled(true);
        } else {
            //not granted permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        mMap.setMyLocationEnabled(true);
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "This app requires location permissions to be granted ", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;

        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationUpdates();

    }

    private void requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG,"Connection suspended");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG,"Connection failed: ConnectionResult.getErrorCode()="+ connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        myLatitude=(float)location.getLatitude();
        myLongitude=(float)location.getLongitude();

    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {

            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(googleApiClient.isConnected()) {
            requestLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }


    private void loadResorts(double lat,double lon){
        StringRequest stringRequest=new StringRequest(Request.Method.GET, RESORT_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray resorts=new JSONArray(response);
                    for(int i=0; i<resorts.length();i++){
                        JSONObject resortObject=resorts.getJSONObject(i);
                        int id=resortObject.getInt("id");
                        String name= resortObject.getString("location");
                        float latitude=Float.parseFloat(resortObject.getString("latitude"));
                        float longitude=Float.parseFloat(resortObject.getString("longitude"));
                        String description=resortObject.getString("description");
                        String program=resortObject.getString("program");
                        String prices=resortObject.getString("prices");
                        String rating1=resortObject.getString("rating");

                        String imageURL=resortObject.getString("imageURL");


                        Resort resort=new Resort(id,name,latitude,longitude,description,program,prices,rating1,imageURL);
                        resortList.add(resort);

                        distante=new double[resortList.size()];

                        for(int j=0;j<distante.length;j++)
                        {
                            distante[j]=CalculationByDistance(resortList.get(j).getLatitude(),resortList.get(j).getLongitude(),lat,lon);
                        }
                        Log.e("DA",String.valueOf(locatiecurenta));
                        addMarkerOnMap(mMap,resortList);


                    }





//Log.e("DA",String.valueOf(resortList.get(0).getLatitude()));
                    resortAdapter=new ResortAdapter(MapActivity.this, resortList,distante);
                    recyclerView.setAdapter(resortAdapter);
                    resortAdapter.setOnItemClickListener(MapActivity.this);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        Volley.newRequestQueue(this).add(stringRequest);
    }

    private void addMarkerOnMap(GoogleMap mMap,ArrayList<Resort> statiune)
    {
        for(int i=0;i<statiune.size();i++)
        {

            MarkerOptions markerOptions= new MarkerOptions();
            LatLng statiune1=new LatLng(statiune.get(i).getLatitude(),statiune.get(i).getLongitude());
            markerOptions.position(statiune1);
            mMap.addMarker(markerOptions).setTitle(statiune.get(i).getLocation());
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent detailIntent=new Intent(this, DetailActivity.class);
        Resort clickedItem=resortList.get(position);
        detailIntent.putExtra("location",clickedItem.getLocation());
        detailIntent.putExtra("description",clickedItem.getDescription());
        detailIntent.putExtra("program",clickedItem.getProgram());
        detailIntent.putExtra("prices", clickedItem.getPrices());
        detailIntent.putExtra("imageURL",clickedItem.getImage());
        startActivity(detailIntent);

    }
    public double CalculationByDistance(double latitude,double longitude,double lat, double lon) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = locatiecurenta.latitude;
        double lat2 = latitude;
        double lon1 = locatiecurenta.longitude;
        double lon2 = longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));

        return Radius * c;
    }
    public void locatie()
    {
        FusedLocationProviderClient fusedLocationClient;
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {

                        locatiecurenta = new LatLng(location.getLatitude(), location.getLongitude());
                        loadResorts(locatiecurenta.latitude,locatiecurenta.longitude);
                    }

                }
            });
        }

    }



}


