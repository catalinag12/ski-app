package com.example.skixpert_trackingskiperformance;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks,
        LocationListener, GoogleApiClient.OnConnectionFailedListener {


    private static final long INTERVAL=1000*2;
    private static final long FASTEST_INTERVAL=1000;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation, lStart, lEnd;
    private final IBinder mBinder=new LocalBinder();
    static double distance;

    public static float speed;

@Nullable
@Override
    public IBinder onBind(Intent intent) {
     createLocationRequest();
     mGoogleApiClient=new GoogleApiClient.Builder(this)
             .addApi(LocationServices.API)
             .addConnectionCallbacks(this)
             .addOnConnectionFailedListener(this)
             .build();
     mGoogleApiClient.connect();
     return mBinder;
}

    @SuppressLint("RestrictedApi")
    private void createLocationRequest() {
        mLocationRequest=new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try{
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,  this);
        }
        catch (SecurityException e)
        {

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        MeasurementsFragment.progressDialog.dismiss();
        mCurrentLocation=location;
        if(lStart==null ){
            lStart=lEnd=mCurrentLocation;
        }
        else{
            lEnd=mCurrentLocation;
        }
        updateUI();
    }


    private void updateUI() {
        if(MeasurementsFragment.p==0) {

            distance = distance + (lStart.distanceTo(lEnd) / 1000.00);
            MeasurementsFragment.endTime = System.currentTimeMillis();
            long diff = MeasurementsFragment.endTime - MeasurementsFragment.startTime;
            diff = TimeUnit.MILLISECONDS.toMinutes(diff);

          //  MeasurementsFragment.tvTime.setText(String.valueOf(diff));
            MeasurementsFragment.tvDistance.setText(new DecimalFormat("0.000").format(distance));
            lStart = lEnd;
        }

    }

    @Override
    public boolean onUnbind(Intent intent) {
    stopLocationUpdates();
    if(mGoogleApiClient.isConnected())
        mGoogleApiClient.disconnect();

    lStart=lEnd=null;
    distance=0;

        return super.onUnbind(intent);
    }

    private void stopLocationUpdates() {
    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    distance=0;
}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public class LocalBinder extends Binder {

        public LocationService getService(){

            return LocationService.this;
        }
    }
}
