package com.example.skixpert_trackingskiperformance.Retrofit;

import com.example.skixpert_trackingskiperformance.Model.MyPlaces;
import com.example.skixpert_trackingskiperformance.Model.PlaceDetail;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface IGoogleApiService {

    @GET
    Call<MyPlaces> getNearbyPlaces(@Url String url);


    @GET
    Call<PlaceDetail> getDetailPlace(@Url String url);

    @GET("maps/api/directions/json")
    Call<String> getDirections(@Query("origin") String origin,
                               @Query("destination") String destination
    );

}
