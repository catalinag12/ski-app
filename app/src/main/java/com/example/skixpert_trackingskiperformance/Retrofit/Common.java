package com.example.skixpert_trackingskiperformance.Retrofit;

import com.example.skixpert_trackingskiperformance.Model.MyPlaces;
import com.example.skixpert_trackingskiperformance.Model.Results;

public class Common {

    public static Results currentResult;
    private static final String GOOGLE_API_URL="https://maps.googleapis.com/";
    public static IGoogleApiService getGoogleApiService()
    {
        return RetrofitClientPlaces.getClient(GOOGLE_API_URL).create(IGoogleApiService.class);
    }

    public static IGoogleApiService getGoogleApiServiceScalars()
    {
        return RetrofitScalarsClient.getScalarClient(GOOGLE_API_URL).create(IGoogleApiService.class);
    }

}
