package com.example.skixpert_trackingskiperformance;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.skixpert_trackingskiperformance.Common.CommonWeather;
import com.example.skixpert_trackingskiperformance.Model_weather.WeatherResult;
import com.example.skixpert_trackingskiperformance.Retrofit.IOpenWeatherMap;
import com.example.skixpert_trackingskiperformance.Retrofit.RetrofitClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.label305.asynctask.SimpleAsyncTask;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;


/**
 * A simple {@link Fragment} subclass.
 */
public class CitiesFragment extends Fragment {

    private List<String> listCities;
    private MaterialSearchBar searchBar;
    ImageView img_weather;
    TextView txt_city, txt_humidity, txt_pressure,txt_temperature, txt_sunset, txt_sunrise,txt_wind,txt_geo_coords, txt_description, txt_date_time;
    LinearLayout weather_panel;
    ProgressBar loading;

    CompositeDisposable compositeDisposable;
    IOpenWeatherMap mService;
    public CitiesFragment() {
        // Required empty public constructor


        compositeDisposable=new CompositeDisposable();
        Retrofit retrofit= RetrofitClient.getInstance();
        mService=retrofit.create(IOpenWeatherMap.class);
    }

    static CitiesFragment instance;

    public static CitiesFragment getInstance(){
        if(instance==null)
            instance=new CitiesFragment();
        return instance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View itemview= inflater.inflate(R.layout.fragment_cities, container, false);

        img_weather=itemview.findViewById(R.id.imgWeather);
        txt_city=itemview.findViewById(R.id.tvCityName);
        txt_pressure=itemview.findViewById(R.id.tv_pressure);
        txt_humidity=itemview.findViewById(R.id.tv_humidity);
        txt_wind=itemview.findViewById(R.id.tv_wind);
        txt_sunrise=itemview.findViewById(R.id.tvSunrise);
        txt_sunset=itemview.findViewById(R.id.tvSunset);
        txt_geo_coords=itemview.findViewById(R.id.tvGeoCoords);
        txt_date_time=itemview.findViewById(R.id.tvDateTime);
        txt_description=itemview.findViewById(R.id.tv_description);
        txt_temperature=itemview.findViewById(R.id.tv_temperature);

        weather_panel=itemview.findViewById(R.id.weather_panel);
        loading=itemview.findViewById(R.id.loading);
        searchBar=itemview.findViewById(R.id.searchBar);
        searchBar.setEnabled(false);
        new LoadCities().execute();

        searchBar.setText("Bucharest");
        return itemview;
    }

    private class LoadCities extends SimpleAsyncTask<List<String>>{

        @Override
        protected List<String> doInBackgroundSimple() {
            listCities=new ArrayList<>();
            try{
                StringBuilder builder=new StringBuilder();
                InputStream is=getResources().openRawResource(R.raw.city_list);
                GZIPInputStream gzipInputStream=new GZIPInputStream(is);
                InputStreamReader reader=new InputStreamReader(gzipInputStream);
                BufferedReader in=new BufferedReader(reader);

                String readed;
                while((readed=in.readLine())!=null)
                    builder.append(readed);

                listCities=new Gson().fromJson(builder.toString(),new TypeToken<List<String>>(){}.getType());

                } catch (IOException e) {
                e.printStackTrace();
            }
            return listCities;
        }

        @Override
        protected void onSuccess(List<String> listCities) {
            super.onSuccess(listCities);
            searchBar.setEnabled(true);
            searchBar.addTextChangeListener(new TextWatcher() {


                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    List<String> suggest=new ArrayList<>();
                    for(String search: listCities){
                        if(search.toLowerCase().contains(searchBar.getText().toLowerCase()))
                            suggest.add(search);
                    }
                    searchBar.setLastSuggestions(suggest);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            searchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
                @Override
                public void onSearchStateChanged(boolean enabled) {

                    Toast.makeText(getContext(),searchBar.getText().toString(),Toast.LENGTH_LONG).show();

                }

                @Override
                public void onSearchConfirmed(CharSequence text) {


                    getWeatherInformation(text.toString());
                    searchBar.setLastSuggestions(listCities);
                }



                @Override
                public void onButtonClicked(int buttonCode) {
                }
            });

            searchBar.setLastSuggestions(listCities);
            loading.setVisibility(View.GONE);
            weather_panel.setVisibility(View.VISIBLE);
        }
    }

    private void getWeatherInformation(String cityName) {
        compositeDisposable.add(mService.getWeatherByCityName(cityName,
                CommonWeather.APP_ID,
                "metric").subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<WeatherResult>() {
                    @Override
                    public void accept(WeatherResult weatherResult) throws Exception {
                        //load image
                        Picasso.get().load(new StringBuilder("https://openweathermap.org/img/w/")
                                .append(weatherResult.getWeather().get(0).getIcon())
                                .append(".png").toString()).into(img_weather);

                        txt_city.setText(weatherResult.getName());
                        txt_description.setText(new StringBuilder("Weather in:").
                                append(weatherResult.getName()).toString());
                        txt_temperature.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getTemp())).append("C").toString());
                        txt_date_time.setText(CommonWeather.convertUnixToDate(weatherResult.getDt()));
                        txt_pressure.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getPressure())).append(" hpa").toString());
                        txt_humidity.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getHumidity())).append("%"));
                        txt_sunrise.setText(CommonWeather.convertUnixToHour(weatherResult.getSys().getSunrise()));
                        txt_sunset.setText(CommonWeather.convertUnixToHour(weatherResult.getSys().getSunset()));
                        txt_geo_coords.setText(new StringBuilder("(").append(weatherResult.getCoord().toString()));
                        txt_wind.setText(new StringBuilder(String.valueOf(weatherResult.getWind().getSpeed())));
                        //display panel

                        weather_panel.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        Toast.makeText(getActivity(),throwable.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }));
    }


    @Override
    public void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    public void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }
}
