package com.example.skixpert_trackingskiperformance;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.skixpert_trackingskiperformance.Model.Photos;
import com.example.skixpert_trackingskiperformance.Model.PlaceDetail;
import com.example.skixpert_trackingskiperformance.Retrofit.Common;
import com.example.skixpert_trackingskiperformance.Retrofit.IGoogleApiService;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewPlace extends AppCompatActivity {

    ImageView photo;
    RatingBar rating_bar;
    TextView opening_hours,place_address,place_name;
    Button btnShopMap,btnShowDirections;

    PlaceDetail myPlace;
    IGoogleApiService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_place);

        photo=findViewById(R.id.photo);

        rating_bar=findViewById(R.id.rating_bar);
        opening_hours=findViewById(R.id.place_open_hour);
        place_address=findViewById(R.id.place_address);
        place_name=findViewById(R.id.place_name );
        btnShopMap=findViewById(R.id.btn_show_map);
        btnShowDirections=findViewById(R.id.btn_show_directions);
        mService=Common.getGoogleApiService();

        place_name.setText("");
        place_address.setText("");
        opening_hours.setText("");

        String lat=Common.currentResult.getGeometry().getLocation().getLat();
        String lng=Common.currentResult.getGeometry().getLocation().getLng();

        Intent intent=new Intent(getApplicationContext(),ViewDirections.class);
        intent.putExtra("lat",lat);
        intent.putExtra("lng",lng);


        btnShopMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent=new Intent(Intent.ACTION_VIEW, Uri.parse(myPlace.getResult().getUrl()));
                startActivity(mapIntent);
            }
        });
        if(Common.currentResult.getPhotos()!=null && Common.currentResult.getPhotos().length>0) {
            Picasso.get().load(getPhotoOfPlace(Common.currentResult.getPhotos()[0].getPhoto_reference(),1000))
                    .placeholder(R.drawable.ic_launcher_background)
                    .into(photo);
        }


        if(Common.currentResult.getRating()!=null && !TextUtils.isEmpty(Common.currentResult.getRating()))
        {
            rating_bar.setRating(Float.parseFloat(Common.currentResult.getRating()));

        }
        else{
            rating_bar.setVisibility(View.GONE);
        }

        if(Common.currentResult.getOpening_hours()!=null  )
        {
            opening_hours.setText(Common.currentResult.getOpening_hours().getOpen_now());
        }
        else{
            opening_hours.setVisibility(View.GONE);
        }

        btnShowDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),lat+lng,Toast.LENGTH_LONG).show();
                Intent mapIntent=new Intent(ViewPlace.this,ViewDirections.class);
                startActivity(mapIntent);
            }
        });


        mService.getDetailPlace(getPlaceDetailUrl(Common.currentResult.getPlace_id()))
                .enqueue(new Callback<PlaceDetail>() {
                    @Override
                    public void onResponse(Call<PlaceDetail> call, Response<PlaceDetail> response) {
                      myPlace=response.body();
                      place_address.setText(myPlace.getResult().getFormatted_address());
                      place_name.setText(myPlace.getResult().getName());
                    }

                    @Override
                    public void onFailure(Call<PlaceDetail> call, Throwable t) {

                    }
                });

    }

    private String getPlaceDetailUrl(String place_id) {
    StringBuilder url=new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json");
    url.append("?placeid="+place_id);
    url.append(("&key="+getResources().getString(R.string.google_maps_key)));
    return url.toString();
    }

    private String getPhotoOfPlace(String photo_reference, int maxWidth) {
    StringBuilder url=new StringBuilder("https://maps.googleapis.com/maps/api/place/photo");
    url.append("?maxwidth="+maxWidth);
    url.append("&photoreference="+photo_reference);
    url.append("&key="+getResources().getString(R.string.google_maps_key));
    return url.toString();

    }
}
