package com.example.skixpert_trackingskiperformance;

import android.content.ClipData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DiaryAdapter extends RecyclerView.Adapter<DiaryAdapter.RecyclerViewAdapter> {

   private Context context;
    private List<Measurements> measurementsList;
    private ItemClickListener itemClickListener;

    public DiaryAdapter(Context context, List<Measurements> measurementsList, ItemClickListener itemClickListener) {
        this.context = context;
        this.measurementsList = measurementsList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_run,
                viewGroup,false);
        return new RecyclerViewAdapter(view,itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter recyclerViewAdapter, int i) {
        Measurements measurements=measurementsList.get(i);
        recyclerViewAdapter.tvIdMeasurement.setText(String.valueOf(measurements.getMeasurementId()));
        recyclerViewAdapter.tvMaximumSpeed.setText(String.valueOf(measurements.getMax_speed()));
//        recyclerViewAdapter.tvAverageSpeed.setText(String.valueOf(measurements.getAvg_speed()));
//        recyclerViewAdapter.tvMaximumAlt.setText(String.valueOf(measurements.getMax_altitude()));
//        recyclerViewAdapter.tvMinimumAlt.setText(String.valueOf(measurements.getMin_altitude()));
        recyclerViewAdapter.tvDist.setText(String.valueOf(measurements.getDistance()));
        recyclerViewAdapter.tvDuration.setText(String.valueOf(measurements.getDuration()));

        if(!MeasurementsFragment.metrics)
        recyclerViewAdapter.typeSpeed.setText("km/h");
        else
            recyclerViewAdapter.typeSpeed.setText("miles/h");
        recyclerViewAdapter.tvDescription.setText(String.valueOf(measurements.getDescription()));
        recyclerViewAdapter.tvRating.setText(String.valueOf(measurements.getRating()));

        if(i%2==1){
        recyclerViewAdapter.card_item.setBackgroundResource(R.color.back);}
        else if(i%3==0){
            recyclerViewAdapter.card_item.setBackgroundResource(R.color.b1);
    }
        else{
            recyclerViewAdapter.card_item.setBackgroundResource(R.color.bluee);
        }
    }

    @Override
    public int getItemCount() {
        return measurementsList.size();
    }

    public void filterList(ArrayList<Measurements> filteredList){
        measurementsList=filteredList;
        notifyDataSetChanged();
    }

    public class RecyclerViewAdapter extends  RecyclerView.ViewHolder implements  View.OnClickListener{
        TextView tvMaximumSpeed,tvAverageSpeed,tvMaximumAlt,tvMinimumAlt,tvDist,tvDuration, tvRating, tvDescription,tvIdMeasurement;
        TextView typeSpeed;
        CardView card_item;
        ItemClickListener itemClickListener;
        public RecyclerViewAdapter(@NonNull View itemView , ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener=itemClickListener;
            tvIdMeasurement=itemView.findViewById(R.id.tvIdMeasurement);

            typeSpeed=itemView.findViewById(R.id.tvTypeSpeed);

            tvMaximumSpeed=itemView.findViewById(R.id.tvMaximumSpeed);
//            tvAverageSpeed=itemView.findViewById(R.id.tvAverageSpeed);
//            tvMaximumAlt=itemView.findViewById(R.id.tvMaximumAlt);
//            tvMinimumAlt=itemView.findViewById(R.id.tvMinimumAlt);
            tvDist=itemView.findViewById(R.id.tvDist);
            tvDuration=itemView.findViewById(R.id.tvDuration);
            tvRating=itemView.findViewById(R.id.tvRating);
            tvDescription=itemView.findViewById(R.id.tvDescription);

            card_item=itemView.findViewById(R.id.card_item);

            this.itemClickListener=itemClickListener;
            card_item.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(v,getAdapterPosition());

        }
    }

   public interface ItemClickListener{
        void onItemClick(View view, int position);
   }
}
