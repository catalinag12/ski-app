package com.example.skixpert_trackingskiperformance;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

public class SettingsActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {

    public static String descriereee;


    public static String url;
    CheckBox cbWeatherNotification;
    Button btnInfo;
    String description;

    TextView tvAlarm;
    TextView tvLunchAlarm;
    Button btnLunchTime, btnCancelLunch,buttonTimePicker,btnBatteryLevel,btnSpeedLevel;



    TextView tvDescriere;
    TextView batteryLevel,speedLevel,tvSlope;
    SeekBar seekBar,seekBarSpeed,seekBarSlope;
    private NotificationManagerCompat notificationManager;
    LocationManager locationManager;
    Location location;
    Boolean click1=false,click2=false;
    public static int battery=20;
    public static int speed=50;
    public static int slope=50;

    private SharedPreferences preferences;
    private static final String PROGRESS_BATTERY="BATTERY";
    private int save;
    private static final String PROGRESS_SPEED="SPEED";
    private static final String PROGRESS_SLOPE="SLOPE";

    private static final String ALARM_WEATHER="WEATHER";
    private static final String ALARM_LUNCH="LUNCH";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);



        seekBarSlope=findViewById(R.id.seekBarSlope);
        tvSlope=findViewById(R.id.tvSlope);
        btnInfo=findViewById(R.id.btnInfo);
        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(SettingsActivity.this);
                alertBuilder.setTitle("DANGER");
                alertBuilder.setMessage("Avalanches require a certain steepness of slope for gravity and weight to overcome friction – typically at least 25 degrees, although shallower slopes can generate avalanches if the snowpack has an exceptionally weak or slippery layer. Very steep mountainsides, meantime, tend to shed snow too regularly to build up snowpacks prone to big slab avalanches. Most avalanches occur on slopes between 35 and 45 degrees.");
                alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                    ;
                });
                AlertDialog ad = alertBuilder.create();
                ad.show();
            }
        });

        seekBarSpeed=findViewById(R.id.seekBarSpeed);
        speedLevel=findViewById(R.id.tvSpeedLevel);

        tvDescriere=findViewById(R.id.tvDescriere);
        notificationManager = NotificationManagerCompat.from(this);

        batteryLevel=findViewById(R.id.tvBatteryLevel);
        seekBar=findViewById(R.id.seekBar);

        tvAlarm=findViewById(R.id.tvAlarm);
        tvLunchAlarm=findViewById(R.id.tvLunchAlarm);
        btnLunchTime=findViewById(R.id.btnLunchTime);
        btnCancelLunch=findViewById(R.id.btnCancelLunchAlarm);


       // descriereee="Bunabuna";

        preferences=getSharedPreferences(" ",MODE_PRIVATE);
        final SharedPreferences.Editor editor=preferences.edit();

        seekBar.setProgress(preferences.getInt(PROGRESS_BATTERY,0));

        seekBarSpeed.setProgress(preferences.getInt(PROGRESS_SPEED,0));

        seekBarSlope.setProgress(preferences.getInt(PROGRESS_SLOPE,0));

        tvAlarm.setText(preferences.getString(ALARM_WEATHER,"No alarm"));
        tvLunchAlarm.setText(preferences.getString(ALARM_LUNCH,"No alarm"));


        tvSlope.setText(String.valueOf(seekBarSlope.getProgress()));

        batteryLevel.setText(String.valueOf(seekBar.getProgress()));
        speedLevel.setText(String.valueOf(seekBarSpeed.getProgress()));
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        location = locationManager.getLastKnownLocation(provider);


        BottomNavigationView bottomNavigationView=findViewById(R.id.bottom_navigation);
        Menu menu=bottomNavigationView.getMenu();
        MenuItem menuItem=menu.getItem(4);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.nav_home:
                        Intent intentHome=new Intent(getApplicationContext(),HomePageActivity.class);
                        startActivity(intentHome);

                        break;

                    case R.id.nav_diary:
                        Intent intent=new Intent(getApplicationContext(),DiaryActivity.class);
                        startActivity(intent);
                        break;


                    case R.id.nav_map:
                        Intent intent2=new Intent(getApplicationContext(),MapActivity.class);
                        startActivity(intent2);
                        break;

                    case R.id.nav_statistics:
                        Intent intent3=new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(intent3);
                        break;

                    case R.id.nav_resorts:
                        Intent intent4=new Intent(getApplicationContext(),ResortActivity.class);
                        startActivity(intent4);
                        break;

                }
                return false;
            }
        });


        findWeather();





         buttonTimePicker=findViewById(R.id.btnTimePicker);
        buttonTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker=new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(),"time picked");
                click1=true;

                editor.putString(ALARM_LUNCH,tvAlarm.getText().toString());
                editor.commit();

            }
        });

        Button buttonCancelAlarm=findViewById(R.id.btnCancelAlarm);
        buttonCancelAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelAlarm();
                Log.d("URL" ,url);
            }
        });


        btnLunchTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker=new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(),"time picked");
                click2=true;

                editor.putString(ALARM_WEATHER,tvAlarm.getText().toString());
                editor.commit();

            }
        });

        btnCancelLunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelAlarmLunch();
            }
        });



        seekBarSlope.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvSlope.setText(progress+"");
                slope=progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                editor.putInt(PROGRESS_SLOPE,seekBar.getProgress());
                editor.commit();
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                batteryLevel.setText(progress+"%");
                battery=progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                editor.putInt(PROGRESS_BATTERY,seekBar.getProgress());
                editor.commit();
            }
        });

        seekBarSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                speedLevel.setText(progress+"km/h");
                speed=progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                editor.putInt(PROGRESS_SPEED,seekBarSpeed.getProgress());
                editor.commit();
            }
        });
    }




    public void findWeather(){

        if(MapActivity.locatiecurenta!=null)
         url="https://openweathermap.org/data/2.5/weather?lat="+MapActivity.locatiecurenta.latitude+"&lon="+MapActivity.locatiecurenta.longitude+"&appid=b6907d289e10d714a6e88b30761fae22";

        JsonObjectRequest jor=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray array=response.getJSONArray("weather");
                    JSONObject object=array.getJSONObject(0);
                    String desc=object.getString("description");
                    descriereee=desc;
                    tvDescriere.setText(desc);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue queue= Volley.newRequestQueue(this);
        queue.add(jor);


    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        Calendar c=Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,hourOfDay);
        c.set(Calendar.MINUTE,minute);
        c.set(Calendar.SECOND,0);

        if(click1){
        updateTimeText(c);
        startAlarm(c);

        click1=false;}
        if(click2){
            updateTimeText2(c);
            startAlarm2(c);
            click2=false;
        }
    }

    private void updateTimeText(Calendar c){
        String timeText="Alarm set for " ;
        timeText+= DateFormat.getTimeInstance(DateFormat.SHORT).format(c.getTime());
        tvAlarm.setText(timeText);


    }

    private void startAlarm(Calendar c){
        AlarmManager alarmManager=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent=new Intent(this,AlertReceiver.class);
        PendingIntent pendingIntent=PendingIntent.getBroadcast(this,1,intent,0);

        if(c.before(Calendar.getInstance())){
            c.add(Calendar.DATE,1);
        }
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(),AlarmManager.INTERVAL_DAY,pendingIntent);
    }
    private void updateTimeText2(Calendar c){
        String timeText="Alarm set for " ;
        timeText+= DateFormat.getTimeInstance(DateFormat.SHORT).format(c.getTime());
        tvLunchAlarm.setText(timeText);
        preferences=getSharedPreferences(" ",MODE_PRIVATE);
        final SharedPreferences.Editor editor=preferences.edit();
        editor.putString(ALARM_LUNCH,timeText).apply();
        editor.commit();
    }

    private void startAlarm2(Calendar c){
        AlarmManager alarmManager=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent=new Intent(this,AlertReceiver2.class);
        PendingIntent pendingIntent=PendingIntent.getBroadcast(this,2,intent,0);

        if(c.before(Calendar.getInstance())){
            c.add(Calendar.DATE,1);
        }
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(),AlarmManager.INTERVAL_DAY,pendingIntent);
    }

    private  void cancelAlarm(){
        AlarmManager alarmManager=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent=new Intent(this,AlertReceiver.class);
        PendingIntent pendingIntent=PendingIntent.getBroadcast(this,1,intent,0);
        alarmManager.cancel(pendingIntent);
        tvAlarm.setText("No alarm");
       // Toast.makeText(getApplicationContext(),descriereee,Toast.LENGTH_LONG).show();
    }
    private  void cancelAlarmLunch(){
        AlarmManager alarmManager=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent=new Intent(this,AlertReceiver.class);
        PendingIntent pendingIntent=PendingIntent.getBroadcast(this,1,intent,0);
        alarmManager.cancel(pendingIntent);
        tvLunchAlarm.setText("No alarm");
        //Toast.makeText(getApplicationContext(),descriereee,Toast.LENGTH_LONG).show();

    }


}
