package com.example.skixpert_trackingskiperformance;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiaryPresenter {

    private DiaryView diaryView;

    public DiaryPresenter(DiaryView diaryView) {
        this.diaryView = diaryView;
    }


    void getData(){
        diaryView.showLoading();
        //request to server
        ApiInterface apiInterface=ApiClient.getApiClient().create(ApiInterface.class);
        Call<List<Measurements>> call=apiInterface.getMeasurements();
        call.enqueue(new Callback<List<Measurements>>() {
            @Override
            public void onResponse(@NonNull Call<List<Measurements>> call, @NonNull Response<List<Measurements>> response) {
                diaryView.hideLoading();
                if(response.isSuccessful() && response.body()!=null) {
                    diaryView.onGetResult(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Measurements>> call,@NonNull Throwable t) {
                diaryView.hideLoading();
                diaryView.onErrorLoading(t.getLocalizedMessage());
            }
        });


    }


}
