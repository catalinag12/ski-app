package com.example.skixpert_trackingskiperformance.Model_weather;

import java.util.List;

public class WeatherForecastResult {

    public String cod;
    public double message;
    public int cnt;
    public List<MyList>list;
    public City city;
}
