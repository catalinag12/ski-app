package com.example.skixpert_trackingskiperformance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText email,password;
    Button btnLogin;
    TextView tvRegister;
    ProgressBar pbLoading;
    CheckBox cbRemember;
    SharedPreferences mPrefs;
    SharedPreferences.Editor editor;
    static String IP="192.168.0.178";

    static String URL_LOGIN="http://"+IP
            +"/licenta/login.php";
    static final String PREFS_NAME="PrefsFile";
    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sessionManager=new SessionManager(this);

        pbLoading=findViewById(R.id.pbLoading);
        btnLogin=findViewById(R.id.btnLogin);
        email=findViewById(R.id.edtEmailLogin);
        password=findViewById(R.id.edtPasswordLogin);
        tvRegister=findViewById(R.id.tvRegister);
        cbRemember=findViewById(R.id.cbRemember);

        mPrefs=getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        getPreferencesData();
        MeasurementsFragment measurementsFragment;



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty((email).getText()) ||  TextUtils.isEmpty(password.getText()) ){

                    if(TextUtils.isEmpty(email.getText())){
                        email.setError("Please insert your email.");
                    }
                    if(TextUtils.isEmpty(password.getText())){
                        password.setError("Please choose a password.");
                    }

                }
                String mEmail=email.getText().toString().trim();
                String mPass=password.getText().toString().trim();
                if(!mEmail.isEmpty() || !mPass.isEmpty()){
                    Login(mEmail,mPass);
                }
                else{
                    email.setError("Please insert email!");
                    password.setError("Please insert password!");
                }
                if(cbRemember.isChecked()){
                    Boolean boolIsChecked=cbRemember.isChecked();
                    SharedPreferences.Editor editor=mPrefs.edit();
                    editor.putString("pref_name",email.getText().toString());
                    editor.putString("pref_pass",password.getText().toString());
                    editor.putBoolean("pref_check",boolIsChecked);
                    editor.apply();
                  //  Toast.makeText(getApplicationContext(),"Settings have been saved",Toast.LENGTH_LONG).show();

                }
                else{
                    mPrefs.edit().clear().apply();
                }

                email.getText().clear();
                password.getText().clear();
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });
    }

    private void getPreferencesData() {
        SharedPreferences sp=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        if(sp.contains("pref_name")) {
            String u=sp.getString("pref_name","not found");
            email.setText(u.toString());
        }

        if(sp.contains("pref_pass")){
            String p=sp.getString("pref_pass","not found");
            password.setText(p.toString());
        }
        if(sp.contains("pref_check")) {
            Boolean b=sp.getBoolean("pref_check",false);
            cbRemember.setChecked(b);
        }
    }

    private void Login(final String email, final String password){
        pbLoading.setVisibility(View.VISIBLE);
        //btnLogin.setVisibility(View.GONE);


        StringRequest stringRequest=new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String success=jsonObject.getString("success");
                            JSONArray jsonArray=jsonObject.getJSONArray("login");

                            if(success.equals("1")){
                                for(int i=0;i<jsonArray.length();i++){
                                    JSONObject object=jsonArray.getJSONObject(i);
                                    String name=object.getString("name").trim();
                                    String email=object.getString("email").trim();
                                    String id=object.getString("id").trim();
                                   // Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();
                                    Toast.makeText(LoginActivity.this,
                                            "Success login. \nYour name: "
                                    +name+"\nYour email: "
                                    +email
                                            +"\n Id : "
                                            +id, Toast.LENGTH_LONG).show();

                                    sessionManager.createSession(name,email,id);

                                    Intent intent=new Intent(getApplicationContext(),HomePageActivity.class);
                                    intent.putExtra("name",name);
                                    intent.putExtra("email",email);
                                    intent.putExtra("user_id",id);

//                                    Intent intent1=new Intent(getApplicationContext(),MeasurementsFragment.class);
//                                    intent1.putExtra("user_id",id);

                                   // get measurement for the user logged in




                                    startActivity(intent);
                                    pbLoading.setVisibility(View.GONE);

                                }
                            }
                            if(success.equals("0")){
                                Toast.makeText(getApplicationContext(),"Wrong email or password",Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(LoginActivity.this," Error" +e.toString(),Toast.LENGTH_LONG).show();
                            pbLoading.setVisibility(View.GONE);
                            btnLogin.setVisibility(View.VISIBLE);

                        }
                    }
                },
                new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this," Error login " +error.toString(),Toast.LENGTH_LONG).show();
                btnLogin.setVisibility(View.VISIBLE);

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
              Map<String,String> params=new HashMap<>();
                params.put("email",email);
                params.put("password",password);

                return params;
            }

        };

         RequestQueue requestQueue= Volley.newRequestQueue(this);
       //stringRequest.setRetryPolicy(new DefaultRetryPolicy( 5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);

    }
}
