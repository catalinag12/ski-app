package com.example.skixpert_trackingskiperformance;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.SimpleTimeZone;

public class DetailActivity extends AppCompatActivity  {

    TextView tvCity, tvLastUpdate, tvDescr, tvDate, tvTemp,tvSnow;
    TextView tvMin,tvMax,tvHumidity,tvWind,tvClouds;
     ImageView imgView;
     String location;
     ImageView imgResort;
     Button btnRate;
     RadioButton rbImpressive, rbSatisftying, rbGood, rbNotSatisfying, rbTerrible;
     int gradeRate;
    RadioButton rb;

    String server_url;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        btnRate=findViewById(R.id.btnRate);
        rbImpressive=findViewById(R.id.rbImpressive);
        rbSatisftying=findViewById(R.id.rbSatisfying);
        rbNotSatisfying=findViewById(R.id.rbNotSatisfying);
        rbGood=findViewById(R.id.rbGood);
        rbTerrible=findViewById(R.id.rbTerrible);

        RadioGroup radioGroup=findViewById(R.id.radioGrup);
      tvCity = findViewById(R.id.tvCity);
      tvDescr = findViewById(R.id.tvDesc);
      //tvDate = findViewById(R.id.tvLastUpdate);
      tvHumidity = findViewById(R.id.tvHumidity);
       tvTemp = findViewById(R.id.tvCelsius);
        imgView = findViewById(R.id.imgWeather);
        tvMax=findViewById(R.id.tvTempMax);
        tvMin=findViewById(R.id.tvTempMin);
        tvWind=findViewById(R.id.tvWind);
        tvClouds=findViewById(R.id.tvClouds);
        imgView=findViewById(R.id.imgWeather);
        tvSnow=findViewById(R.id.tvSnow);




        btnRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int radioId=radioGroup.getCheckedRadioButtonId();

                if (radioId==R.id.rbImpressive) {gradeRate=5;
                rbImpressive.setChecked(true);}
                else if (radioId==R.id.rbSatisfying){ gradeRate=4;
                    rbSatisftying.setChecked(true);}

                else if (radioId==R.id.rbGood) {gradeRate=3;
                    rbGood.setChecked(true);}

                else if (radioId==R.id.rbNotSatisfying) {gradeRate=2;
                    rbNotSatisfying.setChecked(true);}

                else {gradeRate=1;
                    rbTerrible.setChecked(true);}

                server_url="http://192.168.0.178/licenta/update_info_"+location+".php";
                Toast.makeText(getApplicationContext(),"the rating is " +  gradeRate,Toast.LENGTH_LONG).show();
                //Toast.makeText(getApplicationContext(),"the location is " +  server_url,Toast.LENGTH_LONG).show();



                String rating=String.valueOf(gradeRate);


                StringRequest stringRequest=new StringRequest(Request.Method.POST, server_url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try{
                                JSONObject jsonObject=new JSONObject(response);
                                String success=jsonObject.getString("success");

                                if(success.equals("1")){
                                   // Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(),"Error" +e.toString(),Toast.LENGTH_LONG).show();
                            }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"Error" +error.toString(),Toast.LENGTH_LONG).show();

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params=new HashMap<String,String>();
                        params.put("rating",rating);

                        return params;
                    }
                };
                RequestQueue requestQueue= Volley.newRequestQueue(DetailActivity.this);
                requestQueue.add(stringRequest);
            }

        });



        imgResort=findViewById(R.id.image_resort);
        Intent intent = getIntent();
         location = intent.getStringExtra("location");
        String description = intent.getStringExtra("description");
        String program = intent.getStringExtra("program");
        String prices = intent.getStringExtra("prices");
        String imageURL=intent.getStringExtra("imageURL");


        Glide.with(this).load(imageURL).into(imgResort);


        TextView tvLocation = findViewById(R.id.tvDetailLocation);
        TextView tvDescr = findViewById(R.id.tvDetailDescription);
        TextView tvProg = findViewById(R.id.tvDetailProgram);
        TextView tvPrice = findViewById(R.id.tvDetailPrices);

        tvLocation.setText(location);
        tvDescr.setText(description);
        tvPrice.setText(prices);
        tvProg.setText(program);
        findWeather();


    }

    public void findWeather(){
        String url="https://api.openweathermap.org/data/2.5/weather?q="+location+"&appid=a60d609351e91ee66d791cae21d45fe6";

        JsonObjectRequest jor=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject mainObject=response.getJSONObject("main");
                    JSONArray array=response.getJSONArray("weather");
                    JSONObject object=array.getJSONObject(0);

                    String temp=String.valueOf(mainObject.getDouble("temp"));
                    String humidity=String.valueOf(mainObject.getDouble("humidity"));
                    String temp_min=String.valueOf(mainObject.getDouble("temp_min"));
                    String temp_max=String.valueOf(mainObject.getDouble("temp_max"));


                    String description=object.getString("description");
                    String icon=object.getString("icon");

                    JSONObject windObject=response.getJSONObject("wind");
                    JSONObject cloudsObject=response.getJSONObject("clouds");
                    String windSpeed=String.valueOf(windObject.getDouble("speed"));
                    String clouds=String.valueOf(cloudsObject.getDouble("all"));

//

                    String city=response.getString("name");
                    tvTemp.setText(temp +"°C");
                    tvCity.setText(city);
                    tvDescr.setText(description);


                    Calendar calendar=Calendar.getInstance();
                    SimpleDateFormat sdf=new SimpleDateFormat("KKKK-MM-dd");
                    String formatted_date=sdf.format(calendar.getTime());
//                    tvDate.setText(formatted_date);

                    double temp_int=Double.parseDouble(temp);
                    double centi=temp_int;
                    centi=Math.round(centi);
                    int i=(int)centi;
                    tvTemp.setText(String.valueOf(i) +"°C");

                    tvHumidity.setText(humidity + " %");
                    tvMax.setText(temp_max +"°C");
                    tvMin.setText(temp_min +"°C");
                    tvClouds.setText(clouds +" %");
                    tvWind.setText(windSpeed+" m/s");




                    Glide.with(DetailActivity.this).load("https://openweathermap.org/img/w/"+icon+".png").into(imgView);



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue queue= Volley.newRequestQueue(this);
        queue.add(jor);

    }



}
